/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hostelmanagment;

import modules.ConnectionProvider;
import modules.PasswordGenerator;
import modules.SendMail;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Student {
   
    int studentId;
    private String studentName; 
    private String fatherName;
    private String phoneNumber;
    private String email;
    private String roomNumber;
    private String cnicNumber;
    private String address;
    private String college;
    private String status;
    Fee fee=new Fee();
    Room room=new Room();
   Connection con=ConnectionProvider.getCon();

public void addStudent(String _mobileNumber,
        String _studentName,
        String _fatherName,
        String _email,
        String _address,
        String _college,
        String _cnicNumber,
        String _roomNumber,
        String _status){
    this.phoneNumber=_mobileNumber;
    this.studentName=_studentName;
    this.fatherName=_fatherName;
    this.email=_email;
    this.address=_address;
    this.college=_college;
    this.cnicNumber=_cnicNumber;
    this.roomNumber=_roomNumber;
    this.status=_status;
    
   String _password=PasswordGenerator.getAlphaNumericString(12);
    try{
       PreparedStatement ps=con.prepareStatement("insert into student (mobileNo,name,father,email,password,address,college,cnic,roomNo,status) values(?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, phoneNumber);
            ps.setString(2, studentName);
            ps.setString(3, fatherName);
            ps.setString(4, email);
            ps.setString(5, _password);
            ps.setString(6, address);
            ps.setString(7, college);
            ps.setString(8, cnicNumber);
            ps.setString(9, roomNumber);
            ps.setString(10, status);
            ps.executeUpdate();
            room.addStudent(roomNumber);
      
    
    }
    catch(Exception e)
    {
         
          JOptionPane.showMessageDialog(null, e);
    }
     try{
          SendMail.smail(_password,_email);
          JOptionPane.showMessageDialog(null, "Password Has Been Send To "+_email);
     }
     catch(Exception e){
          JOptionPane.showMessageDialog(null, "Send Mail Failed");
     }
    
    
}




public void updateStudent(String _mobileNumber,
        String _studentName,
        String _fatherName,
        String _email,
        String _address,
        String _college,
        String _cnicNumber,
        String _roomNumber,
        String _status,
int _id){
      this.phoneNumber=_mobileNumber;
    this.studentName=_studentName;
    this.fatherName=_fatherName;
    this.email=_email;
    this.address=_address;
    this.college=_college;
    this.cnicNumber=_cnicNumber;
    this.roomNumber=_roomNumber;
    this.status=_status;
    this.studentId=_id;
    try{
        String _oldRoom=null;
           Statement st=con.createStatement();
      ResultSet rs=st.executeQuery("select roomNo from student where id='"+studentId+"'");
           
            if(rs.next())
            {
               _oldRoom = rs.getString("roomNo");
                
            }
            
       PreparedStatement ps=con.prepareStatement("update student set mobileNo=?,name=?,father=?,email=?,address=?,college=?,cnic=?,roomNo=?,status=? where id=?");
            ps.setString(1, phoneNumber);
            ps.setString(2, studentName);
            ps.setString(3, fatherName);
            ps.setString(4, email);
            ps.setString(5, address);
            ps.setString(6, college);
            ps.setString(7, cnicNumber);
            ps.setString(8, roomNumber);
            ps.setString(9, status);
            ps.setInt(10, studentId);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Students Data Updated Successfuly");
      
            room.removeStudent(roomNumber, _oldRoom);
    }
    catch(Exception e)
    {
        
          JOptionPane.showMessageDialog(null, e);
          JOptionPane.showMessageDialog(null, "Error! Cant Update Student Data");
    }
     
    
    
}
public  ResultSet searchStudent(int _id)    {
    this.studentId=_id;
  ResultSet rs=null;
     try{
      
        Statement st=con.createStatement();
        rs=st.executeQuery("Select id as 'Student Id',name as 'Name',father as 'Father',email as 'Email',college as 'College', address as 'Address', cnic as 'CNIC ', mobileNo AS 'Phone' ,roomNo as 'Room No',status AS 'Status' from student where id='"+studentId+"'");
    
         return rs;
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }
 return rs;
}
public  ArrayList<String> getStudentData(int _id)    {
    this.studentId=_id;
  ArrayList<String> result=new ArrayList<String>();
     try{
      
      Statement st=con.createStatement();
      ResultSet rs=st.executeQuery("Select * from student where id='"+studentId+"'");
    if(rs.next()){
        
        result.add(rs.getString("name"));
        result.add(rs.getString("cnic"));
        result.add(rs.getString("father"));
        result.add(rs.getString("mobileNo"));
        result.add(rs.getString("address"));
        result.add(rs.getString("email"));
        result.add(rs.getString("college"));
        result.add(rs.getString("roomNo"));
        result.add(rs.getString("id"));
        result.add(rs.getString("status"));
        
    }
    else{
        JOptionPane.showMessageDialog(null, "Student Not Found");
    }
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }

     return result;
}
public ResultSet displayStudents(String _status)
{
    ResultSet rs=null;
    try{
         Statement st=con.createStatement();
       if(_status=="All")
    {
     
      rs=st.executeQuery("Select id as 'Student Id',name as 'Name',father as 'Father',email as 'Email',college as 'College', address as 'Address', cnic as 'CNIC ', mobileNo AS 'Phone' ,roomNo as 'Room No',status AS 'Status' from student");
    }
       else{
           
           rs=st.executeQuery("Select id as 'Student Id',name as 'Name',father as 'Father',email as 'Email',college as 'College', address as 'Address', cnic as 'CNIC ', mobileNo AS 'Phone' ,roomNo as 'Room No',status AS 'Status' from student where status='"+_status+"'");
           
       }
       
       return rs;
    }
    catch(Exception e)
    {
        
    }
  
    
    return rs;

}



public void deleteStudent(int _id){
         this.studentId=_id;
           try{
        
       java.sql.Statement st=con.createStatement();
       ResultSet rs=st.executeQuery("Select * from student where id='"+studentId+"'");
        
      
          
         if(rs.next()){
             
            PreparedStatement ps=con.prepareStatement("Delete from student where id=?");
            ps.setInt(1, studentId);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, " Student Deleted Successfully");
         }
         else{
          
              JOptionPane.showMessageDialog(null, "Student Not Found");
         }
           
            
          
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Error Cant Delete Student");
        }
    }
  
  
}

