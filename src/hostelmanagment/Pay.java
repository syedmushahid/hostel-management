/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hostelmanagment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.time.LocalDate;
import javax.swing.JOptionPane;
import modules.ConnectionProvider;

/**
 *
 * @author syedm
 */
public class Pay {
    private int employeeId;
    private String employeeName;
    private int amount;
    Connection con=ConnectionProvider.getCon();

    public void payPayment(int _id, int _amount){
           this.employeeId=_id;
           this.amount=_amount;
            
    try{
       PreparedStatement ps=con.prepareStatement("insert into pay(employeeid, amount) values(?,?)");
            
            ps.setInt(1, employeeId);
            ps.setInt(2, amount);
            ps.executeUpdate(); 
            JOptionPane.showMessageDialog(null, "Payment Done To Employee");
    }
    catch(Exception e)
    {
         
          JOptionPane.showMessageDialog(null, e);
    }
        
    }
    public ResultSet displayPayments(){
        ResultSet rs=null;
        
        try{
            
        Statement st=con.createStatement();
        rs=st.executeQuery("select  employee.id as'Employee ID', employee.name as 'Name', pay.date as 'Date',pay.amount as 'Amount/RS'  from pay inner join employee on employee.id=pay.employeeid");
    
         return rs;
            
        }
        catch(Exception e)
        {
             JOptionPane.showMessageDialog(null, e);
        }
        
        return rs;
    }
    public ResultSet paymentHistory(int _id){
        ResultSet rs=null;
        this.employeeId=_id;
        try{
            
        Statement st=con.createStatement();
        rs=st.executeQuery("select  employee.id as'Employee ID', employee.name as 'Name', pay.date as 'Date',pay.amount as 'Amount/RS'  from pay inner join employee on employee.id=pay.employeeid where employee.id='"+employeeId+"'");
    
         return rs;
            
        }
        catch(Exception e)
        {
             JOptionPane.showMessageDialog(null, e);
        }
        
        return rs;
    }
    
}
