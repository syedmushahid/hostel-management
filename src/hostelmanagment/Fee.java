/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hostelmanagment;

import modules.ConnectionProvider;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.time.Month;
import javax.swing.JOptionPane;


/**
 *
 * @author talha
 */
public class Fee {
    Connection con=ConnectionProvider.getCon();
    private int studentId;
    private String expDate;
    private int extraCharges;
    private String month;
    private String status;
   
    public boolean issueChallan(int _id,String _month,String _expDate,int _extraAmount){
    LocalDate currentdate = LocalDate.now();
    LocalDate date=currentdate.plusDays(Integer.parseInt(_expDate));
    
      String expDate=date.toString();
        this.studentId=_id;
        this.expDate=date.toString();
        this.extraCharges=_extraAmount;
        this.month=_month;
        this.status="Un Paid";
        
    
    try{
       PreparedStatement ps=con.prepareStatement("insert into fee(studentId, status, month, expDate, extraCharges) values(?,?,?,?,?)");
            
            ps.setInt(1, studentId);
            ps.setString(2, status);
            ps.setString(3, month);
            ps.setString(4, expDate);
            ps.setInt(5, extraCharges);
            ps.executeUpdate(); 
            return true;
    }
    catch(Exception e)
    {
         
          JOptionPane.showMessageDialog(null, e);
          return false;
    }
}
    public  ResultSet searchFeeChallan(int _id)    {
        this.studentId=_id;
  ResultSet rs=null;
     try{
      
        Statement st=con.createStatement();
        rs=st.executeQuery("Select month as 'Month',status as 'Status' from fee where studentId='"+studentId+"'");
    
         return rs;
         
    }
    catch(SQLException e){
        
        JOptionPane.showMessageDialog(null, e);
    
    }
 return rs;
}
    public void sendFeetoAll(){
          LocalDate currentdate = LocalDate.now();
       LocalDate date=currentdate.plusDays(10);
      Month month= date.getMonth();
      String expMonth=month.toString().trim();
     
        try{
        ResultSet rs;
        Statement st=con.createStatement();
        rs=st.executeQuery("Select id from student where status='living'");
      
        while(rs.next()){
           int _id= Integer.parseInt(rs.getString("id"));
            issueChallan(_id,expMonth,"10",0);
        }
         JOptionPane.showMessageDialog(null, "Fee Challans Has Been Sended To All Students");
        }
       catch(NumberFormatException | SQLException e){
          
             JOptionPane.showMessageDialog(null, e);
     }
        
        
        
    }
    public ResultSet feeReport(String _status){
        ResultSet rs=null;
     try{
      Statement st=con.createStatement();
         if("All".equals(_status))
         {
              
        rs=st.executeQuery("Select student.name as 'Name', student.roomNo as 'Room No',  fee.month as 'Month', room.Fee AS 'Fee',fee.extraCharges as 'Extra Charges', fee.extraCharges+room.Fee as 'Total Amount',fee.expDate as 'Exp Date', fee.status as 'Status' from student INNER join fee on student.id = fee.studentId INNER JOIN room on room.roomNumber = student.roomNo");
    
         }
         else{
              
        rs=st.executeQuery("Select student.name as 'Name', student.roomNo as 'Room No',  fee.month as 'Month', room.Fee AS 'Fee',fee.extraCharges as 'Extra Charges', fee.extraCharges+room.Fee as 'Total Amount',fee.expDate as 'Exp Date', fee.status as 'Status' from student INNER join fee on student.id = fee.studentId INNER JOIN room on room.roomNumber = student.roomNo where fee.status='"+_status+"'");
    
         }
       
         return rs;
         
    }
    catch(Exception e){
         
        JOptionPane.showMessageDialog(null, e);
    
    }
     return rs;
    }

}
