/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hostelmanagment;

import modules.ConnectionProvider;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author syedm
 */
public class Room {
    
    private String roomNumber;
    private String roomStatus;
    private int studentsNum;
    private int fee;
    private int maxBeds;
 
    Connection con=ConnectionProvider.getCon();
  public void addStudent(String _roomNumber){
        this.roomNumber=_roomNumber;
        try{
             PreparedStatement roomUpdate=con.prepareStatement("update room set studentsNumber=studentsNumber+1 where roomNumber=?");
             roomUpdate.setString(1, roomNumber);
             roomUpdate.executeUpdate();
             JOptionPane.showMessageDialog(null, "Student Added Successfuly");
        }
        catch(Exception e){
             JOptionPane.showMessageDialog(null, e);
        }
       
    }
  public void removeStudent(String _newRoom,String _oldRoom){
         try{
             PreparedStatement addStudent=con.prepareStatement("update room set studentsNumber=studentsNumber+1 where roomNumber=?");
              PreparedStatement removeStudent=con.prepareStatement("update room set studentsNumber=studentsNumber-1 where roomNumber=?");
             addStudent.setString(1, _newRoom);
             addStudent.executeUpdate();
             removeStudent.setString(1, _oldRoom);
             removeStudent.executeUpdate();
             
        }
        catch(Exception e){
             JOptionPane.showMessageDialog(null, e);
        }
        
    }
  public  ResultSet displayRooms()    {
  ResultSet rs=null;
     try{
     
        java.sql.Statement st=con.createStatement();
        rs=st.executeQuery("Select roomNumber AS 'Room Number' ,roomStatus as 'Status', maxBeds as 'Max Capactiy', studentsNumber as 'Students',Fee as 'Fee' from room");
        
      
        return rs;
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }
 return rs;
}
  public void addRoom(String _roomNumber, String _roomStatus ,int _maxBeds,int _fee){
        
        this.roomNumber=_roomNumber;
        this.roomStatus=_roomStatus;
        this.maxBeds= _maxBeds;
        this.fee = _fee;
        this.studentsNum=0;
           try{
        
            PreparedStatement ps=con.prepareStatement("insert into room (roomNumber,roomStatus,studentsNumber,maxBeds,Fee) values(?,?,?,?,?)");
            ps.setString(1, roomNumber);
            ps.setString(2, roomStatus);
            ps.setInt(3, studentsNum);
            ps.setInt(4, maxBeds);
            ps.setInt(5, fee);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "New Room successfully Added");
          
        }
        catch(Exception e){
          
            
            JOptionPane.showMessageDialog(null, "Room Is Already Present In DataBase");
        }
    }
  public  ResultSet searchRooms(String _roomNumber)    {
  ResultSet rs=null;
     try{
      
        java.sql.Statement st=con.createStatement();
        rs=st.executeQuery("Select roomNumber AS 'Room Number' ,roomStatus as 'Status', maxBeds as 'Max Capactiy', studentsNumber as 'Students',Fee as 'Fee' from room where roomNumber='"+_roomNumber+"'");
    
         return rs;
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }
 return rs;
}
  public  ArrayList<String> getRoomData(String _roomNumber)    {
  ArrayList<String> result=new ArrayList<String>();
     try{
      
        java.sql.Statement st=con.createStatement();
      ResultSet rs=st.executeQuery("Select * from room where roomNumber='"+_roomNumber+"'");
    if(rs.next()){
        
        result.add(rs.getString("roomNumber"));
        result.add(rs.getString("roomStatus"));
        result.add(rs.getString("maxBeds"));
        result.add(rs.getString("id"));
        result.add(rs.getString("fee"));
        
    }
    else{
        JOptionPane.showMessageDialog(null, "Room Not Found");
    }
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }

     return result;
}
  public void deleteRoom(int _roomid){
         
           try{
        
                java.sql.Statement st=con.createStatement();
       ResultSet rs=st.executeQuery("Select * from room where id='"+_roomid+"'");
        
      
          
         if(rs.next()){
             
             PreparedStatement ps=con.prepareStatement("Delete from room where id=?");
            ps.setInt(1, _roomid);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, " Room Deleted From Database");
         }
         else{
              JOptionPane.showMessageDialog(null, " Room Not Found");
         }
           
            
          
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Room Not Found");
        }
    }
  public void updateRoom(String _roomNumber, String _roomStatus ,int _maxBeds,int _roomid,int _fee){
        
        this.roomNumber=_roomNumber;
        this.roomStatus=_roomStatus;
        this.maxBeds= _maxBeds;
        this.fee=_fee;
   
           try{
        
        
            java.sql.Statement st=con.createStatement();
       ResultSet rs=st.executeQuery("Select * from room where id='"+_roomid+"'");
        
      
          
         if(rs.next()){
            PreparedStatement ps=con.prepareStatement("update room set roomNumber=?,roomStatus=?,maxBeds=?,Fee=? where id=?");
            ps.setString(1, roomNumber);
            ps.setString(2, roomStatus);
            ps.setInt(3, maxBeds);
            ps.setInt(4, fee);
            ps.setInt(5, _roomid);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Room Updated successfully Added");
         }
         else{
              JOptionPane.showMessageDialog(null, "Room Not Found");
         }
        }
        catch(Exception e){
          
               JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Cant Update Invalid Room");
        
         }
    }
  public  ArrayList<String> getavailableRooms()    {
  ArrayList<String> result=new ArrayList<String>();
     try{
      
        java.sql.Statement st=con.createStatement();
      ResultSet rs=st.executeQuery("Select roomNumber from room where maxBeds > studentsNumber AND roomStatus='Active'");
    while(rs.next()){
        
        result.add(rs.getString("roomNumber"));
       
        
    }
  
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }

     return result;
}
  public ResultSet displayRoomMembers(String _roomNo){
        
        ResultSet rs=null;
        
        try{
             java.sql.Statement st=con.createStatement();
       rs=st.executeQuery("Select id as 'Student Id',name as 'Name',father as 'Father',email as 'Email',college as 'College', address as 'Address', cnic as 'CNIC ', mobileNo AS 'Phone' ,roomNo as 'Room No',status AS 'Status' from student where roomNo ='"+_roomNo+"' AND status='living'");
       
           return rs;
       
            
        }
        catch(Exception e){
            
        }
        
        return rs;
        
    }
    
}
