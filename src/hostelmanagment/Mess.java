/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package hostelmanagment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import modules.ConnectionProvider;

/**
 *
 * @author syedm
 */
public class Mess {
    Connection con=ConnectionProvider.getCon();
     
    private String day;
    private String breakfast;
    private String lunch;
    private String dinner;
    public void updateMess(String _day,String _breakfast,String _lunch,String _dinner)
    {
        this.day=_day;
        this.breakfast=_breakfast;
        this.lunch=_lunch;
        this.dinner=_dinner;
       //  JOptionPane.showMessageDialog(null, day);
        try{
              PreparedStatement ps=con.prepareStatement("update mess set breakfast=?,lunch=?,dinner=? where day=?");
            
            ps.setString(1, breakfast);
            ps.setString(2, lunch);
            ps.setString(3, dinner);
            ps.setString(4, day);
            ps.executeUpdate(); 
            JOptionPane.showMessageDialog(null, "Menue Updated");
            
        }
        catch(Exception e){
             JOptionPane.showMessageDialog(null, e);
        }
        
        
    }
    public ArrayList<String> getMessData(String _day){
        
        this.day=_day;
        ArrayList<String> result=new ArrayList<String>();
       
        try{
       Statement st=con.createStatement();
       ResultSet rs=st.executeQuery("Select * from mess where day='"+day+"'");
    
       if(rs.next())
       {
           result.add(rs.getString("day"));
           result.add(rs.getString("breakfast"));
           result.add(rs.getString("lunch"));
           result.add(rs.getString("dinner"));
       }
        }
        catch(Exception e)
        {
             JOptionPane.showMessageDialog(null, e);
        }
       
        return result;
        
        
    }
    public ResultSet displayMenu()
    {
        
        ResultSet rs=null;
        
          try{
       Statement st=con.createStatement();
      rs=st.executeQuery("Select * from mess ");
    
      return rs;
        }
        catch(Exception e)
        {
             JOptionPane.showMessageDialog(null, e);
        }
        
        
        return rs;
    }
}
