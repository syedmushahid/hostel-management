/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package hostelmanagment;

import com.mysql.cj.xdevapi.Statement;
import hostelmanagment.Room;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;
import modules.ConnectionProvider;
import net.proteanit.sql.DbUtils;

/**
 *
 * @author syedm
 */
public class admin extends javax.swing.JFrame {

    /**
     * Creates new form admin
     */ Connection con=ConnectionProvider.getCon();
     
      Room room= new Room();
      Student student=new Student();
      Employee employee=new Employee();
      Mess managemess= new Mess();
      private String adminEmail;
      private String adminPassword;
    private Border defaultBorder;
     
      
      
      
    public admin() {
        initComponents();
        setExtendedState(admin.MAXIMIZED_BOTH); 
loginPanel.setVisible(true);
AdminPanel.setVisible(false);
homeBtnbg.setBackground(new Color(0,153,204));
 viewReport();
 defaultBorder=roomNumberSearch.getBorder();
//editRoomTable.getTableHeader().setOpaque(false);
//editRoomTable.getTableHeader().setBackground(Color.BLACK);


    
    }
    public void mouseClicked(MouseEvent arg0) {
		System.out.println("clicked");}
    private boolean login(String _email,String _password)
    {
        this.adminEmail=_email;
        this.adminPassword=_password;
        try{
            PreparedStatement ps=con.prepareStatement("select * from admin where email=? and password=?");
            ps.setString(1, adminEmail);
            ps.setString(2, adminPassword); 
            ResultSet rs=ps.executeQuery();
            if(rs.next())
            {
loginPanel.setVisible(false);
AdminPanel.setVisible(true);
return true;
            }
        }
        catch(Exception e)
        {
            
        }
        
       
        return false;
    }

    private void logout()
    {
        loginPanel.setVisible(true);
AdminPanel.setVisible(false);
    }
    
    private void viewReport()
    {
        try{
             java.sql.Statement st=con.createStatement();
  java.sql.Statement st1=con.createStatement();
  java.sql.Statement st2=con.createStatement();
ResultSet rs = st.executeQuery("SELECT COUNT(*) AS studentNum FROM student where status='living'");
ResultSet rs1 = st1.executeQuery("SELECT COUNT(*) AS roomNum FROM room");
ResultSet rs2 = st2.executeQuery("SELECT COUNT(*) AS employeeNum FROM employee where status='working'");
String _employee="0";
String _room ="0";
String _students ="0";
if(rs.next())
{
    _students = rs.getString("studentNum"); 
}
rs.close();
if(rs1.next())
{
   _room = rs1.getString("roomNum");
}
rs1.close();
if(rs2.next())
{
    _employee = rs2.getString("employeeNum");
}
rs2.close();

studentsNum.setText(_students);
roomNum.setText(_room);
employeeNum.setText(_employee);



        }
        catch(Exception e){
            
            System.out.print(e);
        }
          
    }
    private void loadroomsTable(){

ResultSet rs = room.displayRooms();
roomsTable.setModel(DbUtils.resultSetToTableModel(rs));
     
  
}
    
    private void changeMenueColor(JPanel active){
       
         homeBtnbg.setBackground(new Color(102,102,102));

        addRoomsBg.setBackground(new Color(102,102,102));
   
        editRoombg.setBackground(new Color(102,102,102));
    
        addStudentbg.setBackground(new Color(102,102,102));
      
        editStudentbg.setBackground(new Color(102,102,102));
       
        studentFeebg.setBackground(new Color(102,102,102));
     
        addEmployeebg.setBackground(new Color(102,102,102));
     
        editemployeebg.setBackground(new Color(102,102,102));

        employeePaymentbg.setBackground(new Color(102,102,102));
   
        searchDetailsbg.setBackground(new Color(102,102,102));
    
        messManagmentbg.setBackground(new Color(102,102,102));
        feeReportbg.setBackground(new Color(102,102,102));
      
        displayStudents.setBackground(new Color(102,102,102));
        
        active.setBackground(new Color(0,153,204));
        
        
    }
    private void resetInputsBorders(){
        
    }
    private void loadEmployeeTable(){
  
ResultSet rs = employee.displayEmployees();
displayEmployee.setModel(DbUtils.resultSetToTableModel(rs));
     
   
} 
  
    private void loadEmployePayTable(){
        ResultSet rs = employee.pay.displayPayments();
employeePayTable.setModel(DbUtils.resultSetToTableModel(rs));
        
    }
    
    private void loadMessMenu()
    {
          ResultSet rs = managemess.displayMenu();
    menuTable.setModel(DbUtils.resultSetToTableModel(rs));
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    
    
   
    private void loadavailableRooms(){
  
       
ArrayList<String> _rooms= room.getavailableRooms();
availableRooms.removeAllItems();
for(int i=0;i<_rooms.size();i++){
    
   availableRooms.addItem(_rooms.get(i));
}
    }
    
    private void clearAddstudentForm(){
       phoneNumber.setText("");
       studentName.setText("");
       fatherName.setText("");
       email.setText("");
       address.setText("");
       college.setText("");
       cnicNumber.setText("");      
    }
    private void clearAddemployeeForm(){
        
       employeePhoneNumber.setText("");
       employeeName.setText("");
       employeeAddress.setText("");    
       employeeCnicNumber.setText("");
              
    }
    private void clearUpdateemployeeForm(){
        
       updateEmployeePhoneNumber.setText("");
       updateEmployeeName.setText("");
       updateEmployeeAddress.setText("");    
       updateEmployeeCnicNumber.setText("");
        employeeId.setText("");
       
      // updateDesignation.setText("");

              
    }
    private void loaddisplayStudentsTable(String _status)
    {
          ResultSet rs = student.displayStudents(_status);
displayStudentsTable.setModel(DbUtils.resultSetToTableModel(rs));
        
    }
      private void loadFeeReportTable(String _status)
    {
          ResultSet rs = student.fee.feeReport(_status);
feeReportTable.setModel(DbUtils.resultSetToTableModel(rs));
        
    }
    private void showDetailsPanel(JPanel _show)
    {
       employeeDetailsPanel.setVisible(false);
       studentDetailsPanel.setVisible(false);
       roomDetailsPanel.setVisible(false);
       _show.setVisible(true);
       
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        loginPanel = new javax.swing.JPanel();
        kGradientPanel1 = new keeptoo.KGradientPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel79 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        loginEmail = new javax.swing.JTextField();
        jLabel80 = new javax.swing.JLabel();
        jLabel87 = new javax.swing.JLabel();
        updateEmployeeFormBtn1 = new javax.swing.JLabel();
        wrongEmailorPass = new javax.swing.JLabel();
        loginPassword = new javax.swing.JPasswordField();
        AdminPanel = new javax.swing.JPanel();
        navigationBar = new javax.swing.JPanel();
        homeBtnbg = new javax.swing.JPanel();
        homeBtn = new javax.swing.JLabel();
        addRoomsBg = new javax.swing.JPanel();
        addRoomsBtn = new javax.swing.JLabel();
        editRoombg = new javax.swing.JPanel();
        editRoomsBtn = new javax.swing.JLabel();
        addStudentbg = new javax.swing.JPanel();
        addStudentBtn = new javax.swing.JLabel();
        editStudentbg = new javax.swing.JPanel();
        editStudentBtn = new javax.swing.JLabel();
        studentFeebg = new javax.swing.JPanel();
        studentFeeBtn = new javax.swing.JLabel();
        addEmployeebg = new javax.swing.JPanel();
        addEmpeloyeBtn = new javax.swing.JLabel();
        editemployeebg = new javax.swing.JPanel();
        editEmloyeeBtn = new javax.swing.JLabel();
        employeePaymentbg = new javax.swing.JPanel();
        employeePaymentBtn = new javax.swing.JLabel();
        searchDetailsbg = new javax.swing.JPanel();
        searchDetailsBtn = new javax.swing.JLabel();
        messManagmentbg = new javax.swing.JPanel();
        messManagment = new javax.swing.JLabel();
        displayStudents = new javax.swing.JPanel();
        livingStudentsBtn = new javax.swing.JLabel();
        logout = new javax.swing.JPanel();
        logoutBtn = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        feeReportbg = new javax.swing.JPanel();
        feeReport = new javax.swing.JLabel();
        mainContent = new javax.swing.JPanel();
        homePage = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jPanel15 = new javax.swing.JPanel();
        studentsNum = new javax.swing.JLabel();
        jLabel89 = new javax.swing.JLabel();
        jPanel16 = new javax.swing.JPanel();
        roomNum = new javax.swing.JLabel();
        jLabel91 = new javax.swing.JLabel();
        jPanel22 = new javax.swing.JPanel();
        employeeNum = new javax.swing.JLabel();
        jLabel93 = new javax.swing.JLabel();
        addStudent = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jLabel30 = new javax.swing.JLabel();
        studentName = new javax.swing.JTextField();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        fatherName = new javax.swing.JTextField();
        jLabel33 = new javax.swing.JLabel();
        phoneNumber = new javax.swing.JTextField();
        email = new javax.swing.JTextField();
        jLabel34 = new javax.swing.JLabel();
        jLabel35 = new javax.swing.JLabel();
        cnicNumber = new javax.swing.JTextField();
        jLabel36 = new javax.swing.JLabel();
        address = new javax.swing.JTextField();
        college = new javax.swing.JTextField();
        jLabel37 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        availableRooms = new javax.swing.JComboBox<>();
        addStudentFormBtn = new javax.swing.JLabel();
        canelAddStudentBtn = new javax.swing.JLabel();
        studentStatus = new javax.swing.JCheckBox();
        editRoom = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        updateRoomNumber = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        updatemaxBeds = new javax.swing.JTextField();
        updateactivateRoom = new javax.swing.JCheckBox();
        updateRoomBtn = new javax.swing.JLabel();
        deleteRoomBtn = new javax.swing.JLabel();
        roomNumberFind = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        findRoom = new javax.swing.JLabel();
        roomId = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        updatemaxBeds1 = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        editRoomTable = new javax.swing.JTable();
        editStudents = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jPanel27 = new javax.swing.JPanel();
        jLabel39 = new javax.swing.JLabel();
        editstudentName = new javax.swing.JTextField();
        jLabel40 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        editfatherName = new javax.swing.JTextField();
        jLabel42 = new javax.swing.JLabel();
        editphoneNumber = new javax.swing.JTextField();
        editemail = new javax.swing.JTextField();
        jLabel43 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        editcnicNumber = new javax.swing.JTextField();
        jLabel45 = new javax.swing.JLabel();
        editaddress = new javax.swing.JTextField();
        editcollege = new javax.swing.JTextField();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        editavailableRooms = new javax.swing.JComboBox<>();
        updateStudentFormBtn = new javax.swing.JLabel();
        clearEditStudentForm = new javax.swing.JLabel();
        studentid = new javax.swing.JLabel();
        studentStatusUpdate = new javax.swing.JCheckBox();
        clearEditStudentForm1 = new javax.swing.JLabel();
        jPanel33 = new javax.swing.JPanel();
        jLabel28 = new javax.swing.JLabel();
        editSearchStudent = new javax.swing.JTextField();
        searchStudentEdit = new javax.swing.JLabel();
        searchDetails = new javax.swing.JPanel();
        jPanel19 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        searchDetailsId = new javax.swing.JTextField();
        searchDetailsby = new javax.swing.JComboBox<>();
        jLabel78 = new javax.swing.JLabel();
        searchDetailsbtn = new javax.swing.JLabel();
        studentDetailsPanel = new javax.swing.JPanel();
        jLabel81 = new javax.swing.JLabel();
        jScrollPane11 = new javax.swing.JScrollPane();
        studentDetailTable = new javax.swing.JTable();
        jLabel82 = new javax.swing.JLabel();
        jScrollPane12 = new javax.swing.JScrollPane();
        studentFeeDetailsTable = new javax.swing.JTable();
        roomDetailsPanel = new javax.swing.JPanel();
        jLabel83 = new javax.swing.JLabel();
        jScrollPane13 = new javax.swing.JScrollPane();
        roomDetailsTable = new javax.swing.JTable();
        jLabel84 = new javax.swing.JLabel();
        jScrollPane14 = new javax.swing.JScrollPane();
        roomMembersDetails = new javax.swing.JTable();
        employeeDetailsPanel = new javax.swing.JPanel();
        jLabel85 = new javax.swing.JLabel();
        jScrollPane15 = new javax.swing.JScrollPane();
        employeDetailsTable = new javax.swing.JTable();
        jLabel86 = new javax.swing.JLabel();
        jScrollPane16 = new javax.swing.JScrollPane();
        employeePaymentDetailsTable = new javax.swing.JTable();
        mess = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        menuDay = new javax.swing.JComboBox<>();
        breakfast = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        lunch = new javax.swing.JTextField();
        dinner = new javax.swing.JTextField();
        jLabel72 = new javax.swing.JLabel();
        updateMenu = new javax.swing.JLabel();
        jLabel74 = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        menuTable = new javax.swing.JTable();
        studentsFee = new javax.swing.JPanel();
        jPanel21 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel31 = new javax.swing.JPanel();
        jLabel27 = new javax.swing.JLabel();
        studentSearch = new javax.swing.JTextField();
        searchStudentFee = new javax.swing.JLabel();
        jPanel32 = new javax.swing.JPanel();
        jLabel62 = new javax.swing.JLabel();
        studentNameFee = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        jLabel64 = new javax.swing.JLabel();
        StudentRoomIdFee = new javax.swing.JTextField();
        challanExpiry = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        jLabel66 = new javax.swing.JLabel();
        studentCnicFee = new javax.swing.JTextField();
        jLabel67 = new javax.swing.JLabel();
        sendChallanToAll = new javax.swing.JLabel();
        cancelFeeBtn = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        studentFeeTable = new javax.swing.JTable();
        jLabel68 = new javax.swing.JLabel();
        extraAmount = new javax.swing.JTextField();
        feeMonth = new javax.swing.JComboBox<>();
        sendFeeBtn = new javax.swing.JLabel();
        feestudentid = new javax.swing.JLabel();
        addEmployee = new javax.swing.JPanel();
        jPanel23 = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        jPanel28 = new javax.swing.JPanel();
        jLabel48 = new javax.swing.JLabel();
        jLabel49 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        employeePhoneNumber = new javax.swing.JTextField();
        jLabel52 = new javax.swing.JLabel();
        jLabel53 = new javax.swing.JLabel();
        employeeCnicNumber = new javax.swing.JTextField();
        jLabel54 = new javax.swing.JLabel();
        employeeAddress = new javax.swing.JTextField();
        jLabel56 = new javax.swing.JLabel();
        designation = new javax.swing.JComboBox<>();
        addEmployeeFormBtn = new javax.swing.JLabel();
        canelAdsEmployeeBtn = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        displayEmployee = new javax.swing.JTable();
        employeeStatus = new javax.swing.JComboBox<>();
        employeeName = new javax.swing.JTextField();
        studentsDisplay = new javax.swing.JPanel();
        jPanel24 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        displayStudentsTable = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        jLabel75 = new javax.swing.JLabel();
        livingStatus = new javax.swing.JComboBox<>();
        jLabel77 = new javax.swing.JLabel();
        searchStudentData = new javax.swing.JTextField();
        searchStudentDatabtn = new javax.swing.JLabel();
        editEmployee = new javax.swing.JPanel();
        jPanel25 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jPanel29 = new javax.swing.JPanel();
        jLabel50 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        updateEmployeePhoneNumber = new javax.swing.JTextField();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        updateEmployeeCnicNumber = new javax.swing.JTextField();
        jLabel60 = new javax.swing.JLabel();
        updateEmployeeAddress = new javax.swing.JTextField();
        jLabel61 = new javax.swing.JLabel();
        updateDesignation = new javax.swing.JComboBox<>();
        clearUpdateemployeeForm = new javax.swing.JLabel();
        canelUpdateEmployeeBtn = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        employeeTable = new javax.swing.JTable();
        employeeId = new javax.swing.JLabel();
        updateemployeeStatus = new javax.swing.JComboBox<>();
        updateEmployeeFormBtn2 = new javax.swing.JLabel();
        updateEmployeeName = new javax.swing.JTextField();
        jPanel30 = new javax.swing.JPanel();
        jLabel24 = new javax.swing.JLabel();
        employeeSearch = new javax.swing.JTextField();
        searchEmployee = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        addRoom = new javax.swing.JPanel();
        jPanel26 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        maxBeds = new javax.swing.JTextField();
        activateRoom = new javax.swing.JCheckBox();
        addNewRoomBtn = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        roomNumber = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        roomFee = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        roomsTable = new javax.swing.JTable();
        jLabel17 = new javax.swing.JLabel();
        roomNumberSearch = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        searchRoom = new javax.swing.JLabel();
        showAllRooms = new javax.swing.JLabel();
        employeepayment = new javax.swing.JPanel();
        jPanel34 = new javax.swing.JPanel();
        jLabel25 = new javax.swing.JLabel();
        jPanel36 = new javax.swing.JPanel();
        jLabel69 = new javax.swing.JLabel();
        employeeidpay = new javax.swing.JTextField();
        jLabel70 = new javax.swing.JLabel();
        jLabel71 = new javax.swing.JLabel();
        employeepayamount = new javax.swing.JTextField();
        jLabel73 = new javax.swing.JLabel();
        cancelFeeBtn1 = new javax.swing.JLabel();
        sendpay = new javax.swing.JLabel();
        feestudentid1 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        employeePayTable = new javax.swing.JTable();
        jLabel76 = new javax.swing.JLabel();
        feeReports = new javax.swing.JPanel();
        jPanel35 = new javax.swing.JPanel();
        jLabel88 = new javax.swing.JLabel();
        jScrollPane9 = new javax.swing.JScrollPane();
        feeReportTable = new javax.swing.JTable();
        jPanel12 = new javax.swing.JPanel();
        jLabel90 = new javax.swing.JLabel();
        feeStatus = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 255));
        setForeground(java.awt.Color.white);
        setResizable(false);
        getContentPane().setLayout(new java.awt.CardLayout());

        loginPanel.setBackground(new java.awt.Color(255, 255, 255));
        loginPanel.setLayout(new java.awt.BorderLayout());

        kGradientPanel1.setkEndColor(new java.awt.Color(0, 153, 204));

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jLabel79.setFont(new java.awt.Font("Calibri", 1, 36)); // NOI18N
        jLabel79.setForeground(new java.awt.Color(0, 153, 204));
        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel79.setText("Admin Panel Login");

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        loginEmail.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        loginEmail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                loginEmailActionPerformed(evt);
            }
        });

        jLabel80.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jLabel80.setText("Enter Email");

        jLabel87.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        jLabel87.setText("Enter Password");

        updateEmployeeFormBtn1.setBackground(new java.awt.Color(0, 153, 204));
        updateEmployeeFormBtn1.setForeground(new java.awt.Color(255, 255, 255));
        updateEmployeeFormBtn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateEmployeeFormBtn1.setText("LOGIN");
        updateEmployeeFormBtn1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        updateEmployeeFormBtn1.setOpaque(true);
        updateEmployeeFormBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateEmployeeFormBtn1MouseClicked(evt);
            }
        });

        wrongEmailorPass.setForeground(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel80)
                            .addComponent(loginEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(loginPassword)
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel87)
                            .addComponent(wrongEmailorPass, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel11Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(updateEmployeeFormBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(wrongEmailorPass, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jLabel80)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loginEmail, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel87)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(loginPassword, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(updateEmployeeFormBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(40, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap(45, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel79, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(45, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(59, 59, 59)
                .addComponent(jLabel79, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(42, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout kGradientPanel1Layout = new javax.swing.GroupLayout(kGradientPanel1);
        kGradientPanel1.setLayout(kGradientPanel1Layout);
        kGradientPanel1Layout.setHorizontalGroup(
            kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kGradientPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        kGradientPanel1Layout.setVerticalGroup(
            kGradientPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, kGradientPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        loginPanel.add(kGradientPanel1, java.awt.BorderLayout.CENTER);

        getContentPane().add(loginPanel, "card2");

        AdminPanel.setBackground(new java.awt.Color(255, 255, 255));
        AdminPanel.setLayout(new java.awt.BorderLayout());

        navigationBar.setBackground(new java.awt.Color(102, 102, 102));
        navigationBar.setAutoscrolls(true);
        navigationBar.setPreferredSize(new java.awt.Dimension(240, 360));

        homeBtnbg.setBackground(new java.awt.Color(102, 102, 102));

        homeBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        homeBtn.setForeground(new java.awt.Color(255, 255, 255));
        homeBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        homeBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/home.png"))); // NOI18N
        homeBtn.setText(" HOME");
        homeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        homeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                homeBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout homeBtnbgLayout = new javax.swing.GroupLayout(homeBtnbg);
        homeBtnbg.setLayout(homeBtnbgLayout);
        homeBtnbgLayout.setHorizontalGroup(
            homeBtnbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, homeBtnbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(homeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        homeBtnbgLayout.setVerticalGroup(
            homeBtnbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(homeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        addRoomsBg.setBackground(new java.awt.Color(102, 102, 102));

        addRoomsBtn.setBackground(new java.awt.Color(255, 255, 51));
        addRoomsBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        addRoomsBtn.setForeground(new java.awt.Color(255, 255, 255));
        addRoomsBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        addRoomsBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/195.png"))); // NOI18N
        addRoomsBtn.setText("Rooms");
        addRoomsBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addRoomsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addRoomsBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout addRoomsBgLayout = new javax.swing.GroupLayout(addRoomsBg);
        addRoomsBg.setLayout(addRoomsBgLayout);
        addRoomsBgLayout.setHorizontalGroup(
            addRoomsBgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addRoomsBgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(addRoomsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addRoomsBgLayout.setVerticalGroup(
            addRoomsBgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(addRoomsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        editRoombg.setBackground(new java.awt.Color(102, 102, 102));

        editRoomsBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        editRoomsBtn.setForeground(new java.awt.Color(255, 255, 255));
        editRoomsBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        editRoomsBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/doc_edit.png"))); // NOI18N
        editRoomsBtn.setText("Edit Room");
        editRoomsBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        editRoomsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editRoomsBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout editRoombgLayout = new javax.swing.GroupLayout(editRoombg);
        editRoombg.setLayout(editRoombgLayout);
        editRoombgLayout.setHorizontalGroup(
            editRoombgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editRoombgLayout.createSequentialGroup()
                .addGap(0, 49, Short.MAX_VALUE)
                .addComponent(editRoomsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        editRoombgLayout.setVerticalGroup(
            editRoombgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editRoomsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        addStudentbg.setBackground(new java.awt.Color(102, 102, 102));

        addStudentBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        addStudentBtn.setForeground(new java.awt.Color(255, 255, 255));
        addStudentBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        addStudentBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/195.png"))); // NOI18N
        addStudentBtn.setText("Add Students");
        addStudentBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addStudentBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addStudentBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout addStudentbgLayout = new javax.swing.GroupLayout(addStudentbg);
        addStudentbg.setLayout(addStudentbgLayout);
        addStudentbgLayout.setHorizontalGroup(
            addStudentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addStudentbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(addStudentBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addStudentbgLayout.setVerticalGroup(
            addStudentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(addStudentBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        editStudentbg.setBackground(new java.awt.Color(102, 102, 102));

        editStudentBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        editStudentBtn.setForeground(new java.awt.Color(255, 255, 255));
        editStudentBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        editStudentBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/doc_edit.png"))); // NOI18N
        editStudentBtn.setText("Edit Students");
        editStudentBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        editStudentBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editStudentBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout editStudentbgLayout = new javax.swing.GroupLayout(editStudentbg);
        editStudentbg.setLayout(editStudentbgLayout);
        editStudentbgLayout.setHorizontalGroup(
            editStudentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editStudentbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(editStudentBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        editStudentbgLayout.setVerticalGroup(
            editStudentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editStudentBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        studentFeebg.setBackground(new java.awt.Color(102, 102, 102));

        studentFeeBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        studentFeeBtn.setForeground(new java.awt.Color(255, 255, 255));
        studentFeeBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        studentFeeBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/cur_dollar.png"))); // NOI18N
        studentFeeBtn.setText("Students Fee");
        studentFeeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        studentFeeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                studentFeeBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout studentFeebgLayout = new javax.swing.GroupLayout(studentFeebg);
        studentFeebg.setLayout(studentFeebgLayout);
        studentFeebgLayout.setHorizontalGroup(
            studentFeebgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, studentFeebgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(studentFeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        studentFeebgLayout.setVerticalGroup(
            studentFeebgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(studentFeeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        addEmployeebg.setBackground(new java.awt.Color(102, 102, 102));

        addEmpeloyeBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        addEmpeloyeBtn.setForeground(new java.awt.Color(255, 255, 255));
        addEmpeloyeBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        addEmpeloyeBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/195.png"))); // NOI18N
        addEmpeloyeBtn.setText("Add Employee");
        addEmpeloyeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addEmpeloyeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addEmpeloyeBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout addEmployeebgLayout = new javax.swing.GroupLayout(addEmployeebg);
        addEmployeebg.setLayout(addEmployeebgLayout);
        addEmployeebgLayout.setHorizontalGroup(
            addEmployeebgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addEmployeebgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(addEmpeloyeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        addEmployeebgLayout.setVerticalGroup(
            addEmployeebgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(addEmpeloyeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        editemployeebg.setBackground(new java.awt.Color(102, 102, 102));

        editEmloyeeBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        editEmloyeeBtn.setForeground(new java.awt.Color(255, 255, 255));
        editEmloyeeBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        editEmloyeeBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/doc_edit.png"))); // NOI18N
        editEmloyeeBtn.setText("Edit Employee");
        editEmloyeeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        editEmloyeeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editEmloyeeBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout editemployeebgLayout = new javax.swing.GroupLayout(editemployeebg);
        editemployeebg.setLayout(editemployeebgLayout);
        editemployeebgLayout.setHorizontalGroup(
            editemployeebgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editemployeebgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(editEmloyeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        editemployeebgLayout.setVerticalGroup(
            editemployeebgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editEmloyeeBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        employeePaymentbg.setBackground(new java.awt.Color(102, 102, 102));

        employeePaymentBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        employeePaymentBtn.setForeground(new java.awt.Color(255, 255, 255));
        employeePaymentBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        employeePaymentBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/cur_dollar.png"))); // NOI18N
        employeePaymentBtn.setText("Employee Payment");
        employeePaymentBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        employeePaymentBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employeePaymentBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout employeePaymentbgLayout = new javax.swing.GroupLayout(employeePaymentbg);
        employeePaymentbg.setLayout(employeePaymentbgLayout);
        employeePaymentbgLayout.setHorizontalGroup(
            employeePaymentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, employeePaymentbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(employeePaymentBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        employeePaymentbgLayout.setVerticalGroup(
            employeePaymentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(employeePaymentBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        searchDetailsbg.setBackground(new java.awt.Color(102, 102, 102));

        searchDetailsBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        searchDetailsBtn.setForeground(new java.awt.Color(255, 255, 255));
        searchDetailsBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        searchDetailsBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/search.png"))); // NOI18N
        searchDetailsBtn.setText("Search Details");
        searchDetailsBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchDetailsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchDetailsBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout searchDetailsbgLayout = new javax.swing.GroupLayout(searchDetailsbg);
        searchDetailsbg.setLayout(searchDetailsbgLayout);
        searchDetailsbgLayout.setHorizontalGroup(
            searchDetailsbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, searchDetailsbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(searchDetailsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        searchDetailsbgLayout.setVerticalGroup(
            searchDetailsbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(searchDetailsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        messManagmentbg.setBackground(new java.awt.Color(102, 102, 102));

        messManagment.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        messManagment.setForeground(new java.awt.Color(255, 255, 255));
        messManagment.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        messManagment.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/doc_lines.png"))); // NOI18N
        messManagment.setText("Mess Managment");
        messManagment.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        messManagment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                messManagmentMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout messManagmentbgLayout = new javax.swing.GroupLayout(messManagmentbg);
        messManagmentbg.setLayout(messManagmentbgLayout);
        messManagmentbgLayout.setHorizontalGroup(
            messManagmentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, messManagmentbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(messManagment, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        messManagmentbgLayout.setVerticalGroup(
            messManagmentbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(messManagment, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        displayStudents.setBackground(new java.awt.Color(102, 102, 102));

        livingStudentsBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        livingStudentsBtn.setForeground(new java.awt.Color(255, 255, 255));
        livingStudentsBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        livingStudentsBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/list_bullets.png"))); // NOI18N
        livingStudentsBtn.setText("Display Students");
        livingStudentsBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        livingStudentsBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                livingStudentsBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout displayStudentsLayout = new javax.swing.GroupLayout(displayStudents);
        displayStudents.setLayout(displayStudentsLayout);
        displayStudentsLayout.setHorizontalGroup(
            displayStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, displayStudentsLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(livingStudentsBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        displayStudentsLayout.setVerticalGroup(
            displayStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(livingStudentsBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        logout.setBackground(new java.awt.Color(102, 102, 102));

        logoutBtn.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        logoutBtn.setForeground(new java.awt.Color(255, 255, 255));
        logoutBtn.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        logoutBtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/top_right_expand.png"))); // NOI18N
        logoutBtn.setText("Logout");
        logoutBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        logoutBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                logoutBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout logoutLayout = new javax.swing.GroupLayout(logout);
        logout.setLayout(logoutLayout);
        logoutLayout.setHorizontalGroup(
            logoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, logoutLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(logoutBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        logoutLayout.setVerticalGroup(
            logoutLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(logoutBtn, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        logo.setFont(new java.awt.Font("Segoe Print", 1, 24)); // NOI18N
        logo.setForeground(new java.awt.Color(255, 255, 255));
        logo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        logo.setText("MY HOSTEL");

        feeReportbg.setBackground(new java.awt.Color(102, 102, 102));

        feeReport.setFont(new java.awt.Font("Segoe UI", 1, 15)); // NOI18N
        feeReport.setForeground(new java.awt.Color(255, 255, 255));
        feeReport.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        feeReport.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/doc_empty.png"))); // NOI18N
        feeReport.setText("Fee Report");
        feeReport.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        feeReport.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                feeReportMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout feeReportbgLayout = new javax.swing.GroupLayout(feeReportbg);
        feeReportbg.setLayout(feeReportbgLayout);
        feeReportbgLayout.setHorizontalGroup(
            feeReportbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, feeReportbgLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(feeReport, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        feeReportbgLayout.setVerticalGroup(
            feeReportbgLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(feeReport, javax.swing.GroupLayout.DEFAULT_SIZE, 42, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout navigationBarLayout = new javax.swing.GroupLayout(navigationBar);
        navigationBar.setLayout(navigationBarLayout);
        navigationBarLayout.setHorizontalGroup(
            navigationBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(homeBtnbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(addRoomsBg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(editRoombg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(addStudentbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(editStudentbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(studentFeebg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(addEmployeebg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(editemployeebg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(employeePaymentbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(searchDetailsbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(messManagmentbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(displayStudents, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(logout, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(logo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(feeReportbg, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        navigationBarLayout.setVerticalGroup(
            navigationBarLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(navigationBarLayout.createSequentialGroup()
                .addComponent(logo, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(homeBtnbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addRoomsBg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editRoombg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addStudentbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editStudentbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(studentFeebg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(addEmployeebg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editemployeebg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(employeePaymentbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchDetailsbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(messManagmentbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(displayStudents, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(feeReportbg, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(logout, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(523, Short.MAX_VALUE))
        );

        AdminPanel.add(navigationBar, java.awt.BorderLayout.LINE_START);

        mainContent.setBackground(new java.awt.Color(255, 255, 255));
        mainContent.setLayout(new java.awt.CardLayout());

        homePage.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(0, 153, 204));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Home Page");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(724, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        studentsNum.setFont(new java.awt.Font("Segoe UI", 1, 110)); // NOI18N
        studentsNum.setForeground(new java.awt.Color(0, 153, 204));
        studentsNum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        studentsNum.setText("0");

        jLabel89.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        jLabel89.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel89.setText("Students");

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(studentsNum, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel89, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(studentsNum, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel89)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        roomNum.setFont(new java.awt.Font("Segoe UI", 1, 110)); // NOI18N
        roomNum.setForeground(new java.awt.Color(0, 153, 204));
        roomNum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        roomNum.setText("0");

        jLabel91.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        jLabel91.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel91.setText("Rooms");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(roomNum, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel91, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(roomNum, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel91)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        employeeNum.setFont(new java.awt.Font("Segoe UI", 1, 110)); // NOI18N
        employeeNum.setForeground(new java.awt.Color(0, 153, 204));
        employeeNum.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        employeeNum.setText("0");

        jLabel93.setFont(new java.awt.Font("Segoe UI", 0, 36)); // NOI18N
        jLabel93.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel93.setText("Employees");

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(employeeNum, javax.swing.GroupLayout.PREFERRED_SIZE, 205, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel93, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(employeeNum, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel93)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout homePageLayout = new javax.swing.GroupLayout(homePage);
        homePage.setLayout(homePageLayout);
        homePageLayout.setHorizontalGroup(
            homePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(homePageLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        homePageLayout.setVerticalGroup(
            homePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(homePageLayout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(173, 173, 173)
                .addGroup(homePageLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(775, Short.MAX_VALUE))
        );

        mainContent.add(homePage, "card2");

        addStudent.setBackground(new java.awt.Color(255, 255, 255));
        addStudent.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addStudentMouseClicked(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(0, 153, 204));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Add New Student");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel30.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel30.setText("Student Name");

        studentName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentNameActionPerformed(evt);
            }
        });

        jLabel31.setBackground(new java.awt.Color(246, 246, 246));
        jLabel31.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("New Student");

        jLabel32.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel32.setText("Father Name");

        fatherName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fatherNameActionPerformed(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel33.setText("Phone Number");

        phoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                phoneNumberActionPerformed(evt);
            }
        });

        email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailActionPerformed(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel34.setText("Email");

        jLabel35.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel35.setText("CNIC Number");

        cnicNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cnicNumberActionPerformed(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel36.setText("Parmanent Address");

        address.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addressActionPerformed(evt);
            }
        });

        college.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                collegeActionPerformed(evt);
            }
        });

        jLabel37.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel37.setText("College Name");

        jLabel38.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel38.setText("Add In Room");

        availableRooms.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        addStudentFormBtn.setBackground(new java.awt.Color(0, 153, 204));
        addStudentFormBtn.setForeground(new java.awt.Color(255, 255, 255));
        addStudentFormBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addStudentFormBtn.setText("ADD");
        addStudentFormBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addStudentFormBtn.setOpaque(true);
        addStudentFormBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addStudentFormBtnMouseClicked(evt);
            }
        });

        canelAddStudentBtn.setBackground(new java.awt.Color(255, 51, 51));
        canelAddStudentBtn.setForeground(new java.awt.Color(255, 255, 255));
        canelAddStudentBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        canelAddStudentBtn.setText("Cancel");
        canelAddStudentBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        canelAddStudentBtn.setOpaque(true);
        canelAddStudentBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                canelAddStudentBtnMouseClicked(evt);
            }
        });

        studentStatus.setText("Student Is Living in Hostel");
        studentStatus.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentStatusActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel31, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(studentStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel32)
                            .addComponent(fatherName, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel30)
                            .addComponent(studentName, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel33)
                            .addComponent(phoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel34))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel35)
                            .addComponent(cnicNumber)
                            .addComponent(jLabel36)
                            .addComponent(address)
                            .addComponent(college)
                            .addComponent(jLabel37)
                            .addComponent(jLabel38)
                            .addComponent(availableRooms, 0, 328, Short.MAX_VALUE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(221, 221, 221)
                        .addComponent(canelAddStudentBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(addStudentFormBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel31, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(studentName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel32)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(fatherName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel33)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(phoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel34)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(email, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(availableRooms, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel35)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cnicNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel36)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(address, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel37)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(college, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel38)
                        .addGap(42, 42, 42)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 8, Short.MAX_VALUE)
                .addComponent(studentStatus)
                .addGap(18, 18, 18)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addStudentFormBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(canelAddStudentBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );

        javax.swing.GroupLayout addStudentLayout = new javax.swing.GroupLayout(addStudent);
        addStudent.setLayout(addStudentLayout);
        addStudentLayout.setHorizontalGroup(
            addStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(addStudentLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        addStudentLayout.setVerticalGroup(
            addStudentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addStudentLayout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(61, 61, 61)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mainContent.add(addStudent, "card2");

        editRoom.setBackground(new java.awt.Color(255, 255, 255));
        editRoom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editRoomMouseClicked(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(0, 153, 204));

        jLabel5.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Edit Rooms Data");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(724, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel18.setText(" Room Number");

        updateRoomNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateRoomNumberActionPerformed(evt);
            }
        });

        jLabel19.setText("Max Capacity");

        updatemaxBeds.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatemaxBedsActionPerformed(evt);
            }
        });

        updateactivateRoom.setBackground(new java.awt.Color(255, 255, 255));
        updateactivateRoom.setText("Activate");

        updateRoomBtn.setBackground(new java.awt.Color(0, 153, 204));
        updateRoomBtn.setForeground(new java.awt.Color(255, 255, 255));
        updateRoomBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateRoomBtn.setText("Update");
        updateRoomBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        updateRoomBtn.setOpaque(true);
        updateRoomBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateRoomBtnMouseClicked(evt);
            }
        });

        deleteRoomBtn.setBackground(new java.awt.Color(255, 51, 51));
        deleteRoomBtn.setForeground(new java.awt.Color(255, 255, 255));
        deleteRoomBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        deleteRoomBtn.setText("Delete");
        deleteRoomBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        deleteRoomBtn.setOpaque(true);
        deleteRoomBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deleteRoomBtnMouseClicked(evt);
            }
        });

        roomNumberFind.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                roomNumberFindActionPerformed(evt);
            }
        });

        jLabel20.setText("Enter Room Number");

        findRoom.setBackground(new java.awt.Color(0, 153, 204));
        findRoom.setForeground(new java.awt.Color(255, 255, 255));
        findRoom.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        findRoom.setText("Find Rom");
        findRoom.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        findRoom.setOpaque(true);
        findRoom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                findRoomMouseClicked(evt);
            }
        });

        jLabel23.setText("Fee Per Student");

        updatemaxBeds1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updatemaxBeds1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap(62, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(updateRoomNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel20, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(roomNumberFind, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(findRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel19)
                            .addComponent(updateactivateRoom)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(jLabel18))
                            .addComponent(jLabel23))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(roomId))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel3Layout.createSequentialGroup()
                        .addComponent(deleteRoomBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(updateRoomBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(updatemaxBeds, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(updatemaxBeds1, javax.swing.GroupLayout.Alignment.LEADING))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(jLabel20)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(roomNumberFind, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(findRoom, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addComponent(jLabel18)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updateRoomNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel19)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatemaxBeds, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel23)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(updatemaxBeds1, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(updateactivateRoom)
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(updateRoomBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(deleteRoomBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roomId)
                .addContainerGap(39, Short.MAX_VALUE))
        );

        editRoomTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        editRoomTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Room Number", "Status", "Max Capacity", "Students", "Fee"
            }
        ));
        editRoomTable.setFocusable(false);
        editRoomTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        editRoomTable.setRowHeight(30);
        editRoomTable.setSelectionBackground(new java.awt.Color(0, 153, 204));
        editRoomTable.setShowVerticalLines(false);
        editRoomTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(editRoomTable);

        javax.swing.GroupLayout editRoomLayout = new javax.swing.GroupLayout(editRoom);
        editRoom.setLayout(editRoomLayout);
        editRoomLayout.setHorizontalGroup(
            editRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(editRoomLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(editRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 709, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(editRoomLayout.createSequentialGroup()
                        .addGap(185, 185, 185)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        editRoomLayout.setVerticalGroup(
            editRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editRoomLayout.createSequentialGroup()
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(652, Short.MAX_VALUE))
        );

        mainContent.add(editRoom, "card2");

        editStudents.setBackground(new java.awt.Color(255, 255, 255));
        editStudents.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editStudentsMouseClicked(evt);
            }
        });

        jPanel18.setBackground(new java.awt.Color(0, 153, 204));

        jLabel6.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Edit Sudents Data");

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(724, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel39.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel39.setText("Student Name");

        editstudentName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editstudentNameActionPerformed(evt);
            }
        });

        jLabel40.setBackground(new java.awt.Color(246, 246, 246));
        jLabel40.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel40.setText("Edit Student Data");

        jLabel41.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel41.setText("Father Name");

        editfatherName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editfatherNameActionPerformed(evt);
            }
        });

        jLabel42.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel42.setText("Phone Number");

        editphoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editphoneNumberActionPerformed(evt);
            }
        });

        editemail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editemailActionPerformed(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel43.setText("Email");

        jLabel44.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel44.setText("CNIC Number");

        editcnicNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editcnicNumberActionPerformed(evt);
            }
        });

        jLabel45.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel45.setText("Parmanent Address");

        editaddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editaddressActionPerformed(evt);
            }
        });

        editcollege.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editcollegeActionPerformed(evt);
            }
        });

        jLabel46.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel46.setText("College Name");

        jLabel47.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel47.setText("Room");

        updateStudentFormBtn.setBackground(new java.awt.Color(0, 153, 204));
        updateStudentFormBtn.setForeground(new java.awt.Color(255, 255, 255));
        updateStudentFormBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateStudentFormBtn.setText("Update");
        updateStudentFormBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        updateStudentFormBtn.setOpaque(true);
        updateStudentFormBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateStudentFormBtnMouseClicked(evt);
            }
        });

        clearEditStudentForm.setBackground(new java.awt.Color(102, 102, 102));
        clearEditStudentForm.setForeground(new java.awt.Color(255, 255, 255));
        clearEditStudentForm.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        clearEditStudentForm.setText("Clear");
        clearEditStudentForm.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        clearEditStudentForm.setOpaque(true);
        clearEditStudentForm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clearEditStudentFormMouseClicked(evt);
            }
        });

        studentStatusUpdate.setText("Student Is Living in Hostel");
        studentStatusUpdate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentStatusUpdateActionPerformed(evt);
            }
        });

        clearEditStudentForm1.setBackground(new java.awt.Color(255, 0, 0));
        clearEditStudentForm1.setForeground(new java.awt.Color(255, 255, 255));
        clearEditStudentForm1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        clearEditStudentForm1.setText("Delete");
        clearEditStudentForm1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        clearEditStudentForm1.setOpaque(true);
        clearEditStudentForm1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clearEditStudentForm1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel27Layout = new javax.swing.GroupLayout(jPanel27);
        jPanel27.setLayout(jPanel27Layout);
        jPanel27Layout.setHorizontalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(studentid))
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel27Layout.createSequentialGroup()
                                .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 492, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(clearEditStudentForm1, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel27Layout.createSequentialGroup()
                                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel41)
                                    .addComponent(editfatherName, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel39)
                                    .addComponent(editstudentName, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel42)
                                    .addComponent(editphoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(editemail, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel43)
                                    .addComponent(studentStatusUpdate, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(clearEditStudentForm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel44)
                                    .addComponent(editcnicNumber)
                                    .addComponent(jLabel45)
                                    .addComponent(editaddress)
                                    .addComponent(editcollege)
                                    .addComponent(jLabel46)
                                    .addComponent(jLabel47)
                                    .addComponent(editavailableRooms, 0, 328, Short.MAX_VALUE)
                                    .addComponent(updateStudentFormBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel27Layout.setVerticalGroup(
            jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel27Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clearEditStudentForm1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addComponent(jLabel39)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editstudentName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel41)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editfatherName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editphoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel43)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(editemail, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editavailableRooms, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel27Layout.createSequentialGroup()
                        .addComponent(jLabel44)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editcnicNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel45)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editaddress, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel46)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(editcollege, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel47)
                        .addGap(42, 42, 42)))
                .addComponent(studentid)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 5, Short.MAX_VALUE)
                .addComponent(studentStatusUpdate)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel27Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(updateStudentFormBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(clearEditStudentForm, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );

        jLabel28.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel28.setText("Search Student ");

        editSearchStudent.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                editSearchStudentInputMethodTextChanged(evt);
            }
        });
        editSearchStudent.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editSearchStudentActionPerformed(evt);
            }
        });

        searchStudentEdit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/243.png"))); // NOI18N
        searchStudentEdit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchStudentEdit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchStudentEditMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel33Layout = new javax.swing.GroupLayout(jPanel33);
        jPanel33.setLayout(jPanel33Layout);
        jPanel33Layout.setHorizontalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel33Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel28)
                .addGap(18, 18, 18)
                .addComponent(editSearchStudent, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchStudentEdit)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel33Layout.setVerticalGroup(
            jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel33Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchStudentEdit)
                    .addGroup(jPanel33Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(editSearchStudent, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel28)))
                .addGap(22, 22, 22))
        );

        javax.swing.GroupLayout editStudentsLayout = new javax.swing.GroupLayout(editStudents);
        editStudents.setLayout(editStudentsLayout);
        editStudentsLayout.setHorizontalGroup(
            editStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel18, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, editStudentsLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(editStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel33, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel27, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        editStudentsLayout.setVerticalGroup(
            editStudentsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editStudentsLayout.createSequentialGroup()
                .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jPanel33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jPanel27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainContent.add(editStudents, "card2");

        searchDetails.setBackground(new java.awt.Color(255, 255, 255));
        searchDetails.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchDetailsMouseClicked(evt);
            }
        });

        jPanel19.setBackground(new java.awt.Color(0, 153, 204));

        jLabel7.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Search Details");

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(724, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        searchDetailsId.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchDetailsIdActionPerformed(evt);
            }
        });

        searchDetailsby.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Student", "Room", "Employee", "" }));
        searchDetailsby.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchDetailsbyActionPerformed(evt);
            }
        });

        jLabel78.setText("Enter Employee Id/Student Id or Room Number");

        searchDetailsbtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/243.png"))); // NOI18N
        searchDetailsbtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchDetailsbtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchDetailsbtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(searchDetailsby, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(searchDetailsId, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel78, javax.swing.GroupLayout.Alignment.TRAILING))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(searchDetailsbtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel78)
                        .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(searchDetailsby, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel9Layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(searchDetailsId))))
                    .addComponent(searchDetailsbtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        studentDetailsPanel.setOpaque(false);

        jLabel81.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel81.setText("Student Details");

        studentDetailTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        studentDetailTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        studentDetailTable.setRowHeight(30);
        studentDetailTable.setShowVerticalLines(false);
        jScrollPane11.setViewportView(studentDetailTable);

        jLabel82.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel82.setText("Student Fee Details");

        studentFeeDetailsTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        studentFeeDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        studentFeeDetailsTable.setRowHeight(30);
        studentFeeDetailsTable.setShowVerticalLines(false);
        jScrollPane12.setViewportView(studentFeeDetailsTable);

        javax.swing.GroupLayout studentDetailsPanelLayout = new javax.swing.GroupLayout(studentDetailsPanel);
        studentDetailsPanel.setLayout(studentDetailsPanelLayout);
        studentDetailsPanelLayout.setHorizontalGroup(
            studentDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studentDetailsPanelLayout.createSequentialGroup()
                .addGroup(studentDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(studentDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(studentDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel81, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel82, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(studentDetailsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(studentDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane12)
                            .addComponent(jScrollPane11))))
                .addContainerGap())
        );
        studentDetailsPanelLayout.setVerticalGroup(
            studentDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studentDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel81)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane11, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel82)
                .addGap(12, 12, 12)
                .addComponent(jScrollPane12, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        roomDetailsPanel.setOpaque(false);

        jLabel83.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel83.setText("Room Details");

        roomDetailsTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        roomDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        roomDetailsTable.setRowHeight(30);
        roomDetailsTable.setShowVerticalLines(false);
        jScrollPane13.setViewportView(roomDetailsTable);

        jLabel84.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel84.setText("Room Member Details");

        roomMembersDetails.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        roomMembersDetails.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        roomMembersDetails.setRowHeight(30);
        roomMembersDetails.setShowVerticalLines(false);
        jScrollPane14.setViewportView(roomMembersDetails);

        javax.swing.GroupLayout roomDetailsPanelLayout = new javax.swing.GroupLayout(roomDetailsPanel);
        roomDetailsPanel.setLayout(roomDetailsPanelLayout);
        roomDetailsPanelLayout.setHorizontalGroup(
            roomDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, roomDetailsPanelLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel84)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(roomDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(roomDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(roomDetailsPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel83)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane13, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane14))
                .addContainerGap())
        );
        roomDetailsPanelLayout.setVerticalGroup(
            roomDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(roomDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel83)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane13, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel84)
                .addGap(12, 12, 12)
                .addComponent(jScrollPane14, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        employeeDetailsPanel.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        employeeDetailsPanel.setOpaque(false);

        jLabel85.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel85.setText("Employee Details");

        employeDetailsTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        employeDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        employeDetailsTable.setRowHeight(30);
        employeDetailsTable.setShowVerticalLines(false);
        jScrollPane15.setViewportView(employeDetailsTable);

        jLabel86.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel86.setText("Employee Payment Details");

        employeePaymentDetailsTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        employeePaymentDetailsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        employeePaymentDetailsTable.setRowHeight(30);
        employeePaymentDetailsTable.setShowVerticalLines(false);
        jScrollPane16.setViewportView(employeePaymentDetailsTable);

        javax.swing.GroupLayout employeeDetailsPanelLayout = new javax.swing.GroupLayout(employeeDetailsPanel);
        employeeDetailsPanel.setLayout(employeeDetailsPanelLayout);
        employeeDetailsPanelLayout.setHorizontalGroup(
            employeeDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeeDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(employeeDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1015, Short.MAX_VALUE)
                    .addComponent(jScrollPane15, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(employeeDetailsPanelLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(employeeDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel85)
                            .addComponent(jLabel86))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        employeeDetailsPanelLayout.setVerticalGroup(
            employeeDetailsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeeDetailsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel85)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane15, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel86)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane16, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout searchDetailsLayout = new javax.swing.GroupLayout(searchDetails);
        searchDetails.setLayout(searchDetailsLayout);
        searchDetailsLayout.setHorizontalGroup(
            searchDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(searchDetailsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(searchDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(employeeDetailsPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(searchDetailsLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(studentDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(roomDetailsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        searchDetailsLayout.setVerticalGroup(
            searchDetailsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(searchDetailsLayout.createSequentialGroup()
                .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(studentDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(employeeDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(roomDetailsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mainContent.add(searchDetails, "card2");

        mess.setBackground(new java.awt.Color(255, 255, 255));

        jPanel20.setBackground(new java.awt.Color(0, 153, 204));

        jLabel8.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Mess Managment");

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        menuDay.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Monday", "Tuesday", "Wednesday", "Throusday", "Friday", "Saturday", "Sunday" }));
        menuDay.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                menuDayItemStateChanged(evt);
            }
        });

        breakfast.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                breakfastActionPerformed(evt);
            }
        });

        jLabel10.setText("Breakfast");

        jLabel29.setText("Lunch");

        lunch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lunchActionPerformed(evt);
            }
        });

        dinner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                dinnerActionPerformed(evt);
            }
        });

        jLabel72.setText("Dinner");

        updateMenu.setBackground(new java.awt.Color(0, 153, 204));
        updateMenu.setForeground(new java.awt.Color(255, 255, 255));
        updateMenu.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateMenu.setText("Update");
        updateMenu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        updateMenu.setOpaque(true);
        updateMenu.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateMenuMouseClicked(evt);
            }
        });

        jLabel74.setText("Select Day");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(menuDay, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(breakfast)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel10)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(0, 106, Short.MAX_VALUE)
                        .addComponent(updateMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 106, Short.MAX_VALUE))
                    .addComponent(lunch)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel29)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel72)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(dinner)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(jLabel74)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(29, Short.MAX_VALUE))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel74)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(menuDay, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(breakfast, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel29)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lunch, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel72)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dinner, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(updateMenu, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
        );

        menuTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        menuTable.setRowHeight(47);
        jScrollPane6.setViewportView(menuTable);

        javax.swing.GroupLayout messLayout = new javax.swing.GroupLayout(mess);
        mess.setLayout(messLayout);
        messLayout.setHorizontalGroup(
            messLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(messLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 668, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        messLayout.setVerticalGroup(
            messLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(messLayout.createSequentialGroup()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(118, 118, 118)
                .addGroup(messLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mainContent.add(mess, "card2");

        studentsFee.setBackground(new java.awt.Color(255, 255, 255));
        studentsFee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                studentsFeeMouseClicked(evt);
            }
        });

        jPanel21.setBackground(new java.awt.Color(0, 153, 204));

        jLabel9.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Student Fee");

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(724, Short.MAX_VALUE))
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel27.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel27.setText("Search Student ");

        studentSearch.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                studentSearchInputMethodTextChanged(evt);
            }
        });
        studentSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentSearchActionPerformed(evt);
            }
        });

        searchStudentFee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/243.png"))); // NOI18N
        searchStudentFee.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchStudentFee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchStudentFeeMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel31Layout = new javax.swing.GroupLayout(jPanel31);
        jPanel31.setLayout(jPanel31Layout);
        jPanel31Layout.setHorizontalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addContainerGap(102, Short.MAX_VALUE)
                .addComponent(jLabel27)
                .addGap(18, 18, 18)
                .addComponent(studentSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(searchStudentFee, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(102, Short.MAX_VALUE))
        );
        jPanel31Layout.setVerticalGroup(
            jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel31Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel31Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(studentSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel27))
                    .addComponent(searchStudentFee))
                .addGap(24, 24, 24))
        );

        jLabel62.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel62.setText("Student Name");

        studentNameFee.setText(" ");
        studentNameFee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentNameFeeActionPerformed(evt);
            }
        });

        jLabel63.setBackground(new java.awt.Color(246, 246, 246));
        jLabel63.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel63.setText("Student Fees");

        jLabel64.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel64.setText("Room Id");

        StudentRoomIdFee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StudentRoomIdFeeActionPerformed(evt);
            }
        });

        challanExpiry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                challanExpiryActionPerformed(evt);
            }
        });

        jLabel65.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel65.setText("Expires In");

        jLabel66.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel66.setText("CNIC Number");

        studentCnicFee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studentCnicFeeActionPerformed(evt);
            }
        });

        jLabel67.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel67.setText("Month");

        sendChallanToAll.setBackground(new java.awt.Color(0, 153, 204));
        sendChallanToAll.setForeground(new java.awt.Color(255, 255, 255));
        sendChallanToAll.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sendChallanToAll.setText("Send To All");
        sendChallanToAll.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sendChallanToAll.setOpaque(true);
        sendChallanToAll.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sendChallanToAllMouseClicked(evt);
            }
        });

        cancelFeeBtn.setBackground(new java.awt.Color(255, 51, 51));
        cancelFeeBtn.setForeground(new java.awt.Color(255, 255, 255));
        cancelFeeBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancelFeeBtn.setText("Cancel");
        cancelFeeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancelFeeBtn.setOpaque(true);
        cancelFeeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelFeeBtnMouseClicked(evt);
            }
        });

        studentFeeTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        studentFeeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "month ", "Status"
            }
        ));
        studentFeeTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        studentFeeTable.setRowHeight(30);
        jScrollPane5.setViewportView(studentFeeTable);

        jLabel68.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel68.setText("Extra Amount");

        extraAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                extraAmountActionPerformed(evt);
            }
        });

        feeMonth.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        feeMonth.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "January", "February", "March", "April", "May", "June", "July ", "August", "October", "November", "December" }));

        sendFeeBtn.setBackground(new java.awt.Color(0, 153, 204));
        sendFeeBtn.setForeground(new java.awt.Color(255, 255, 255));
        sendFeeBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sendFeeBtn.setText("Send");
        sendFeeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sendFeeBtn.setOpaque(true);
        sendFeeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sendFeeBtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel32Layout = new javax.swing.GroupLayout(jPanel32);
        jPanel32.setLayout(jPanel32Layout);
        jPanel32Layout.setHorizontalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel32Layout.createSequentialGroup()
                        .addGap(0, 28, Short.MAX_VALUE)
                        .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(cancelFeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(studentNameFee, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel64)
                                .addComponent(StudentRoomIdFee, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(challanExpiry, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel65)
                                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(feestudentid)
                                    .addComponent(jLabel62))))
                        .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel32Layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel66)
                                    .addComponent(studentCnicFee, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                                    .addComponent(jLabel67)
                                    .addComponent(jLabel68)
                                    .addComponent(extraAmount, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                                    .addComponent(feeMonth, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(jPanel32Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(sendFeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 703, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(sendChallanToAll, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44))
        );
        jPanel32Layout.setVerticalGroup(
            jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel32Layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel63, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(feestudentid))
                    .addComponent(sendChallanToAll, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel32Layout.createSequentialGroup()
                        .addComponent(jLabel62)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(studentNameFee, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jLabel64)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(StudentRoomIdFee, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(feeMonth, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel32Layout.createSequentialGroup()
                        .addComponent(jLabel66)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(studentCnicFee, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel67)
                        .addGap(41, 41, 41)))
                .addGap(12, 12, 12)
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel65)
                    .addComponent(jLabel68))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(challanExpiry, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(extraAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel32Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelFeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sendFeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout studentsFeeLayout = new javax.swing.GroupLayout(studentsFee);
        studentsFee.setLayout(studentsFeeLayout);
        studentsFeeLayout.setHorizontalGroup(
            studentsFeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel21, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(studentsFeeLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(studentsFeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel32, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel31, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        studentsFeeLayout.setVerticalGroup(
            studentsFeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studentsFeeLayout.createSequentialGroup()
                .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(jPanel31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        mainContent.add(studentsFee, "card2");

        addEmployee.setBackground(new java.awt.Color(255, 255, 255));
        addEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addEmployeeMouseClicked(evt);
            }
        });

        jPanel23.setBackground(new java.awt.Color(0, 153, 204));

        jLabel11.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Add Employee");

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel48.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel48.setText("Employee Name");

        jLabel49.setBackground(new java.awt.Color(246, 246, 246));
        jLabel49.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel49.setText("New Employee");

        jLabel51.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel51.setText("Phone Number");

        employeePhoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeePhoneNumberActionPerformed(evt);
            }
        });

        jLabel52.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel52.setText("Employee Status");

        jLabel53.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel53.setText("CNIC Number");

        employeeCnicNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeCnicNumberActionPerformed(evt);
            }
        });

        jLabel54.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel54.setText("Parmanent Address");

        employeeAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeAddressActionPerformed(evt);
            }
        });

        jLabel56.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel56.setText("Designation");

        designation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Maintainance", "Cook", "Security", "Finance", "Management" }));

        addEmployeeFormBtn.setBackground(new java.awt.Color(0, 153, 204));
        addEmployeeFormBtn.setForeground(new java.awt.Color(255, 255, 255));
        addEmployeeFormBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addEmployeeFormBtn.setText("ADD");
        addEmployeeFormBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addEmployeeFormBtn.setOpaque(true);
        addEmployeeFormBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addEmployeeFormBtnMouseClicked(evt);
            }
        });

        canelAdsEmployeeBtn.setBackground(new java.awt.Color(255, 51, 51));
        canelAdsEmployeeBtn.setForeground(new java.awt.Color(255, 255, 255));
        canelAdsEmployeeBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        canelAdsEmployeeBtn.setText("Cancel");
        canelAdsEmployeeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        canelAdsEmployeeBtn.setOpaque(true);
        canelAdsEmployeeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                canelAdsEmployeeBtnMouseClicked(evt);
            }
        });

        displayEmployee.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        displayEmployee.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Name", "Phone Number", "CNIC Number", "Address", "Email", "Designation"
            }
        ));
        displayEmployee.setIntercellSpacing(new java.awt.Dimension(0, 0));
        displayEmployee.setRowHeight(30);
        displayEmployee.setShowVerticalLines(false);
        jScrollPane4.setViewportView(displayEmployee);

        employeeStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Working", "Not working" }));

        employeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeNameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel28Layout = new javax.swing.GroupLayout(jPanel28);
        jPanel28.setLayout(jPanel28Layout);
        jPanel28Layout.setHorizontalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel49, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel48)
                    .addComponent(jLabel51)
                    .addComponent(employeePhoneNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                    .addComponent(jLabel52)
                    .addComponent(employeeStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(employeeName, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel53)
                    .addComponent(employeeCnicNumber)
                    .addComponent(jLabel54)
                    .addComponent(employeeAddress)
                    .addComponent(jLabel56)
                    .addComponent(designation, 0, 328, Short.MAX_VALUE))
                .addContainerGap(23, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(canelAdsEmployeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(addEmployeeFormBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4)
                .addContainerGap())
        );
        jPanel28Layout.setVerticalGroup(
            jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel28Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel53)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(employeeCnicNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel54)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(employeeAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel48)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(employeeName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jLabel51)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(employeePhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel56)
                        .addGap(42, 42, 42))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel28Layout.createSequentialGroup()
                        .addComponent(jLabel52)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(designation, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
                            .addComponent(employeeStatus))))
                .addGap(30, 30, 30)
                .addGroup(jPanel28Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addEmployeeFormBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(canelAdsEmployeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout addEmployeeLayout = new javax.swing.GroupLayout(addEmployee);
        addEmployee.setLayout(addEmployeeLayout);
        addEmployeeLayout.setHorizontalGroup(
            addEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, addEmployeeLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        addEmployeeLayout.setVerticalGroup(
            addEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addEmployeeLayout.createSequentialGroup()
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63)
                .addComponent(jPanel28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainContent.add(addEmployee, "card2");

        studentsDisplay.setBackground(new java.awt.Color(255, 255, 255));

        jPanel24.setBackground(new java.awt.Color(0, 153, 204));

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 255, 255));
        jLabel12.setText("Living Students");

        javax.swing.GroupLayout jPanel24Layout = new javax.swing.GroupLayout(jPanel24);
        jPanel24.setLayout(jPanel24Layout);
        jPanel24Layout.setHorizontalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel24Layout.setVerticalGroup(
            jPanel24Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel24Layout.createSequentialGroup()
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        displayStudentsTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        displayStudentsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        displayStudentsTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        displayStudentsTable.setRowHeight(30);
        displayStudentsTable.setShowVerticalLines(false);
        jScrollPane8.setViewportView(displayStudentsTable);

        jLabel75.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel75.setText("Display By ");

        livingStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "All", "Living ", "Not Living", " " }));
        livingStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                livingStatusItemStateChanged(evt);
            }
        });

        jLabel77.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel77.setText("Search By ID");

        searchStudentData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                searchStudentDataActionPerformed(evt);
            }
        });

        searchStudentDatabtn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/243.png"))); // NOI18N
        searchStudentDatabtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchStudentDatabtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchStudentDatabtnMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel75)
                .addGap(26, 26, 26)
                .addComponent(livingStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel77)
                .addGap(18, 18, 18)
                .addComponent(searchStudentData, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchStudentDatabtn, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(searchStudentDatabtn)
                    .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel75)
                        .addComponent(livingStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel77)
                        .addComponent(searchStudentData, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(36, 36, 36))
        );

        javax.swing.GroupLayout studentsDisplayLayout = new javax.swing.GroupLayout(studentsDisplay);
        studentsDisplay.setLayout(studentsDisplayLayout);
        studentsDisplayLayout.setHorizontalGroup(
            studentsDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel24, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, studentsDisplayLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(studentsDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        studentsDisplayLayout.setVerticalGroup(
            studentsDisplayLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studentsDisplayLayout.createSequentialGroup()
                .addComponent(jPanel24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.DEFAULT_SIZE, 1114, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainContent.add(studentsDisplay, "card2");

        editEmployee.setBackground(new java.awt.Color(255, 255, 255));
        editEmployee.setAutoscrolls(true);
        editEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                editEmployeeMouseClicked(evt);
            }
        });

        jPanel25.setBackground(new java.awt.Color(0, 153, 204));

        jLabel13.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Edit Employee Data");

        javax.swing.GroupLayout jPanel25Layout = new javax.swing.GroupLayout(jPanel25);
        jPanel25.setLayout(jPanel25Layout);
        jPanel25Layout.setHorizontalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 418, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel25Layout.setVerticalGroup(
            jPanel25Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel25Layout.createSequentialGroup()
                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel50.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel50.setText("Employee Name");

        jLabel55.setBackground(new java.awt.Color(246, 246, 246));
        jLabel55.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("Update Employee");

        jLabel57.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel57.setText("Phone Number");

        updateEmployeePhoneNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateEmployeePhoneNumberActionPerformed(evt);
            }
        });

        jLabel58.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel58.setText("Employee Status");

        jLabel59.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel59.setText("CNIC Number");

        updateEmployeeCnicNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateEmployeeCnicNumberActionPerformed(evt);
            }
        });

        jLabel60.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel60.setText("Parmanent Address");

        updateEmployeeAddress.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateEmployeeAddressActionPerformed(evt);
            }
        });

        jLabel61.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel61.setText("Designation");

        updateDesignation.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Maintainance", "Cook", "Security", "Finance", "Management" }));

        clearUpdateemployeeForm.setBackground(new java.awt.Color(102, 102, 102));
        clearUpdateemployeeForm.setForeground(new java.awt.Color(255, 255, 255));
        clearUpdateemployeeForm.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        clearUpdateemployeeForm.setText("Cancel");
        clearUpdateemployeeForm.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        clearUpdateemployeeForm.setOpaque(true);
        clearUpdateemployeeForm.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                clearUpdateemployeeFormMouseClicked(evt);
            }
        });

        canelUpdateEmployeeBtn.setBackground(new java.awt.Color(255, 51, 51));
        canelUpdateEmployeeBtn.setForeground(new java.awt.Color(255, 255, 255));
        canelUpdateEmployeeBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        canelUpdateEmployeeBtn.setText("Delete");
        canelUpdateEmployeeBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        canelUpdateEmployeeBtn.setOpaque(true);
        canelUpdateEmployeeBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                canelUpdateEmployeeBtnMouseClicked(evt);
            }
        });

        employeeTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        employeeTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Mobile Number", "Employee Name", "Email", "Address", "CNIC", "Designation"
            }
        ));
        employeeTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        employeeTable.setRowHeight(30);
        employeeTable.setShowVerticalLines(false);
        jScrollPane3.setViewportView(employeeTable);
        if (employeeTable.getColumnModel().getColumnCount() > 0) {
            employeeTable.getColumnModel().getColumn(1).setHeaderValue("Status");
            employeeTable.getColumnModel().getColumn(2).setHeaderValue("Email");
            employeeTable.getColumnModel().getColumn(3).setHeaderValue("Address");
            employeeTable.getColumnModel().getColumn(4).setHeaderValue("CNIC");
            employeeTable.getColumnModel().getColumn(5).setHeaderValue("Designation");
        }

        updateemployeeStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Working", "Not Working" }));

        updateEmployeeFormBtn2.setBackground(new java.awt.Color(0, 153, 204));
        updateEmployeeFormBtn2.setForeground(new java.awt.Color(255, 255, 255));
        updateEmployeeFormBtn2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        updateEmployeeFormBtn2.setText("Update");
        updateEmployeeFormBtn2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        updateEmployeeFormBtn2.setOpaque(true);
        updateEmployeeFormBtn2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                updateEmployeeFormBtn2MouseClicked(evt);
            }
        });

        updateEmployeeName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateEmployeeNameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel29Layout = new javax.swing.GroupLayout(jPanel29);
        jPanel29.setLayout(jPanel29Layout);
        jPanel29Layout.setHorizontalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 703, Short.MAX_VALUE))
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel29Layout.createSequentialGroup()
                                .addGap(29, 29, 29)
                                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel50)
                                        .addComponent(jLabel57)
                                        .addComponent(updateEmployeePhoneNumber, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
                                        .addComponent(jLabel58)
                                        .addGroup(jPanel29Layout.createSequentialGroup()
                                            .addGap(90, 90, 90)
                                            .addComponent(employeeId, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(updateemployeeStatus, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(updateEmployeeName, javax.swing.GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE))
                                    .addComponent(clearUpdateemployeeForm, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel59)
                                    .addComponent(updateEmployeeCnicNumber)
                                    .addComponent(jLabel60)
                                    .addComponent(updateEmployeeAddress)
                                    .addComponent(jLabel61)
                                    .addComponent(updateDesignation, 0, 328, Short.MAX_VALUE)
                                    .addComponent(updateEmployeeFormBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel29Layout.createSequentialGroup()
                                .addGap(129, 129, 129)
                                .addComponent(jLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                                .addComponent(canelUpdateEmployeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel29Layout.setVerticalGroup(
            jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel29Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel55, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(canelUpdateEmployeeBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(58, 58, 58)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addComponent(jLabel59)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateEmployeeCnicNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel60)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateEmployeeAddress, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel29Layout.createSequentialGroup()
                        .addComponent(jLabel50)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateEmployeeName, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(jLabel57)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(updateEmployeePhoneNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel29Layout.createSequentialGroup()
                        .addComponent(jLabel61)
                        .addGap(42, 42, 42))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel29Layout.createSequentialGroup()
                        .addComponent(jLabel58)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(updateDesignation, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(updateemployeeStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addComponent(employeeId, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addGroup(jPanel29Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(clearUpdateemployeeForm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(updateEmployeeFormBtn2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        jLabel24.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel24.setText("Search Employee ");

        employeeSearch.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                employeeSearchInputMethodTextChanged(evt);
            }
        });
        employeeSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeSearchActionPerformed(evt);
            }
        });

        searchEmployee.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/243.png"))); // NOI18N
        searchEmployee.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchEmployee.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchEmployeeMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel30Layout = new javax.swing.GroupLayout(jPanel30);
        jPanel30.setLayout(jPanel30Layout);
        jPanel30Layout.setHorizontalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel30Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel24)
                .addGap(18, 18, 18)
                .addComponent(employeeSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(searchEmployee)
                .addGap(86, 86, 86))
        );
        jPanel30Layout.setVerticalGroup(
            jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel30Layout.createSequentialGroup()
                .addContainerGap(32, Short.MAX_VALUE)
                .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel30Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(employeeSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel24))
                    .addComponent(searchEmployee))
                .addGap(24, 24, 24))
        );

        javax.swing.GroupLayout editEmployeeLayout = new javax.swing.GroupLayout(editEmployee);
        editEmployee.setLayout(editEmployeeLayout);
        editEmployeeLayout.setHorizontalGroup(
            editEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel25, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(editEmployeeLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(editEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel30, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel29, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        editEmployeeLayout.setVerticalGroup(
            editEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(editEmployeeLayout.createSequentialGroup()
                .addComponent(jPanel25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(39, 39, 39)
                .addComponent(jPanel30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(editEmployeeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(editEmployeeLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel26))
                    .addGroup(editEmployeeLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainContent.add(editEmployee, "card2");

        addRoom.setBackground(new java.awt.Color(255, 255, 255));
        addRoom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addRoomMouseClicked(evt);
            }
        });

        jPanel26.setBackground(new java.awt.Color(0, 153, 204));

        jLabel14.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 255, 255));
        jLabel14.setText("Add New Room");

        javax.swing.GroupLayout jPanel26Layout = new javax.swing.GroupLayout(jPanel26);
        jPanel26.setLayout(jPanel26Layout);
        jPanel26Layout.setHorizontalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel26Layout.setVerticalGroup(
            jPanel26Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel26Layout.createSequentialGroup()
                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        maxBeds.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                maxBedsActionPerformed(evt);
            }
        });

        activateRoom.setBackground(new java.awt.Color(255, 255, 255));
        activateRoom.setText("Activate");

        addNewRoomBtn.setBackground(new java.awt.Color(0, 153, 204));
        addNewRoomBtn.setForeground(new java.awt.Color(255, 255, 255));
        addNewRoomBtn.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        addNewRoomBtn.setText("ADD");
        addNewRoomBtn.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        addNewRoomBtn.setOpaque(true);
        addNewRoomBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                addNewRoomBtnMouseClicked(evt);
            }
        });

        jLabel3.setText("Enter Room Number");

        jLabel15.setText("Enter Max Capacity");

        roomNumber.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                roomNumberActionPerformed(evt);
            }
        });

        jLabel22.setText("Fee Per  Student");

        roomFee.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                roomFeeActionPerformed(evt);
            }
        });

        jLabel21.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel21.setText("New Room");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(46, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(addNewRoomBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel15)
                    .addComponent(maxBeds, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22)
                    .addComponent(roomFee, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(roomNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(activateRoom)
                    .addComponent(jLabel21))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel21)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 22, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roomNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel15)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(maxBeds, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jLabel22)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(roomFee, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(activateRoom)
                .addGap(18, 18, 18)
                .addComponent(addNewRoomBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
        );

        roomsTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        roomsTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        roomsTable.setFocusable(false);
        roomsTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        roomsTable.setRowHeight(30);
        roomsTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        roomsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        roomsTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        roomsTable.setShowVerticalLines(false);
        roomsTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(roomsTable);

        jLabel17.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("Curent Rooms");

        roomNumberSearch.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                roomNumberSearchInputMethodTextChanged(evt);
            }
        });
        roomNumberSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                roomNumberSearchActionPerformed(evt);
            }
        });

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel16.setText("Search Room ");

        searchRoom.setIcon(new javax.swing.ImageIcon(getClass().getResource("/hostelmanagment/icons/243.png"))); // NOI18N
        searchRoom.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        searchRoom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                searchRoomMouseClicked(evt);
            }
        });

        showAllRooms.setBackground(new java.awt.Color(0, 153, 204));
        showAllRooms.setForeground(new java.awt.Color(255, 255, 255));
        showAllRooms.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        showAllRooms.setText("Show All Rooms");
        showAllRooms.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        showAllRooms.setOpaque(true);
        showAllRooms.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                showAllRoomsMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout addRoomLayout = new javax.swing.GroupLayout(addRoom);
        addRoom.setLayout(addRoomLayout);
        addRoomLayout.setHorizontalGroup(
            addRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel26, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(addRoomLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(addRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addGroup(addRoomLayout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(showAllRooms, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(addRoomLayout.createSequentialGroup()
                        .addComponent(roomNumberSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(searchRoom))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        addRoomLayout.setVerticalGroup(
            addRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(addRoomLayout.createSequentialGroup()
                .addComponent(jPanel26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(searchRoom)
                    .addComponent(roomNumberSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(addRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(showAllRooms, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(addRoomLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 512, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainContent.add(addRoom, "card2");

        employeepayment.setBackground(new java.awt.Color(255, 255, 255));
        employeepayment.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                employeepaymentMouseClicked(evt);
            }
        });

        jPanel34.setBackground(new java.awt.Color(0, 153, 204));

        jLabel25.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(255, 255, 255));
        jLabel25.setText("Employee Payment");

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addComponent(jLabel25, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jLabel69.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel69.setText("Employee ID");

        employeeidpay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeeidpayActionPerformed(evt);
            }
        });

        jLabel70.setBackground(new java.awt.Color(246, 246, 246));
        jLabel70.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel70.setText("Employee Payment");

        jLabel71.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel71.setText("Enter Amount In RS");

        employeepayamount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                employeepayamountActionPerformed(evt);
            }
        });

        jLabel73.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N

        cancelFeeBtn1.setBackground(new java.awt.Color(255, 51, 51));
        cancelFeeBtn1.setForeground(new java.awt.Color(255, 255, 255));
        cancelFeeBtn1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        cancelFeeBtn1.setText("Cancel");
        cancelFeeBtn1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        cancelFeeBtn1.setOpaque(true);
        cancelFeeBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cancelFeeBtn1MouseClicked(evt);
            }
        });

        sendpay.setBackground(new java.awt.Color(0, 153, 204));
        sendpay.setForeground(new java.awt.Color(255, 255, 255));
        sendpay.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        sendpay.setText("Send");
        sendpay.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        sendpay.setOpaque(true);
        sendpay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sendpayMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel36Layout = new javax.swing.GroupLayout(jPanel36);
        jPanel36.setLayout(jPanel36Layout);
        jPanel36Layout.setHorizontalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel36Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel71)
                            .addComponent(employeeidpay, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel69)
                            .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel36Layout.createSequentialGroup()
                                    .addComponent(cancelFeeBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(sendpay, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(employeepayamount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel36Layout.createSequentialGroup()
                        .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel36Layout.createSequentialGroup()
                                .addComponent(jLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, 331, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(feestudentid1))
                            .addGroup(jPanel36Layout.createSequentialGroup()
                                .addGap(679, 679, 679)
                                .addComponent(jLabel73)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel36Layout.setVerticalGroup(
            jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel36Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel70, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(feestudentid1)
                .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel36Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel73)
                        .addGap(240, 240, 240))
                    .addGroup(jPanel36Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel69)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(employeeidpay, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(2, 2, 2)
                        .addComponent(jLabel71)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(employeepayamount, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel36Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cancelFeeBtn1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(sendpay, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        employeePayTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        employeePayTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        employeePayTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        employeePayTable.setRowHeight(30);
        employeePayTable.setShowVerticalLines(false);
        jScrollPane7.setViewportView(employeePayTable);

        jLabel76.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel76.setText("All Time Payments");

        javax.swing.GroupLayout employeepaymentLayout = new javax.swing.GroupLayout(employeepayment);
        employeepayment.setLayout(employeepaymentLayout);
        employeepaymentLayout.setHorizontalGroup(
            employeepaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(employeepaymentLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(employeepaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane7)
                    .addComponent(jLabel76, javax.swing.GroupLayout.PREFERRED_SIZE, 581, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        employeepaymentLayout.setVerticalGroup(
            employeepaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(employeepaymentLayout.createSequentialGroup()
                .addComponent(jPanel34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(jLabel76, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(employeepaymentLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 443, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jPanel36, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        mainContent.add(employeepayment, "card2");

        feeReports.setBackground(new java.awt.Color(255, 255, 255));

        jPanel35.setBackground(new java.awt.Color(0, 153, 204));

        jLabel88.setFont(new java.awt.Font("Segoe UI", 1, 36)); // NOI18N
        jLabel88.setForeground(new java.awt.Color(255, 255, 255));
        jLabel88.setText("Fee Report");

        javax.swing.GroupLayout jPanel35Layout = new javax.swing.GroupLayout(jPanel35);
        jPanel35.setLayout(jPanel35Layout);
        jPanel35Layout.setHorizontalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addComponent(jLabel88, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel35Layout.setVerticalGroup(
            jPanel35Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel35Layout.createSequentialGroup()
                .addComponent(jLabel88, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        feeReportTable.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        feeReportTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        feeReportTable.setIntercellSpacing(new java.awt.Dimension(0, 0));
        feeReportTable.setRowHeight(30);
        feeReportTable.setShowVerticalLines(false);
        jScrollPane9.setViewportView(feeReportTable);

        jLabel90.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel90.setText("Display By ");

        feeStatus.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "All", "Paid", "Unpaid", " " }));
        feeStatus.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                feeStatusItemStateChanged(evt);
            }
        });

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel90)
                .addGap(26, 26, 26)
                .addComponent(feeStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(799, Short.MAX_VALUE))
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel12Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel90)
                    .addComponent(feeStatus, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(36, 36, 36))
        );

        javax.swing.GroupLayout feeReportsLayout = new javax.swing.GroupLayout(feeReports);
        feeReports.setLayout(feeReportsLayout);
        feeReportsLayout.setHorizontalGroup(
            feeReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel35, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, feeReportsLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(feeReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1088, Short.MAX_VALUE)
                    .addComponent(jPanel12, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        feeReportsLayout.setVerticalGroup(
            feeReportsLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(feeReportsLayout.createSequentialGroup()
                .addComponent(jPanel35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel12, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane9, javax.swing.GroupLayout.DEFAULT_SIZE, 1114, Short.MAX_VALUE)
                .addContainerGap())
        );

        mainContent.add(feeReports, "card2");

        AdminPanel.add(mainContent, java.awt.BorderLayout.CENTER);

        getContentPane().add(AdminPanel, "card2");

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void clearStudentFee(){
       DefaultTableModel model=(DefaultTableModel) studentFeeTable.getModel();
        studentSearch.setText("");
        studentNameFee.setText("");
        studentCnicFee.setText("");
        StudentRoomIdFee.setText("");
        challanExpiry.setText("");
        extraAmount.setText("");
     model.setRowCount(0);
    }
    
    private void homeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_homeBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(homePage);
mainContent.repaint();
mainContent.revalidate();
changeMenueColor(homeBtnbg);
viewReport();

    }//GEN-LAST:event_homeBtnMouseClicked

    private void addRoomsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addRoomsBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();


loadroomsTable();
mainContent.add(addRoom);
mainContent.repaint();
mainContent.revalidate();// 
changeMenueColor(addRoomsBg);

      // addRoomsBg.setBackground(new Color(255,255,255));
    }//GEN-LAST:event_addRoomsBtnMouseClicked

    private void editRoomsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editRoomsBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(editRoom);
mainContent.repaint();
mainContent.revalidate();//  
changeMenueColor(editRoombg);// TODO add your handling code here:
    }//GEN-LAST:event_editRoomsBtnMouseClicked

    private void addStudentBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addStudentBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(addStudent);
mainContent.repaint();
mainContent.revalidate();
changeMenueColor(addStudentbg);

loadavailableRooms();
 

    }//GEN-LAST:event_addStudentBtnMouseClicked

    private void editStudentBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editStudentBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(editStudents);
mainContent.repaint();
mainContent.revalidate();// 
changeMenueColor(editStudentbg);// TODO add your handling code here:
    }//GEN-LAST:event_editStudentBtnMouseClicked

    private void studentFeeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_studentFeeBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(studentsFee);
mainContent.repaint();
mainContent.revalidate();//
changeMenueColor(studentFeebg);// TODO add your handling code here:
    }//GEN-LAST:event_studentFeeBtnMouseClicked

    private void addEmpeloyeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addEmpeloyeBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(addEmployee);
mainContent.repaint();
mainContent.revalidate();//   
loadEmployeeTable();
changeMenueColor(addEmployeebg);




// TODO add your handling code here:
    }//GEN-LAST:event_addEmpeloyeBtnMouseClicked

    private void editEmloyeeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editEmloyeeBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(editEmployee);
mainContent.repaint();
mainContent.revalidate();//
changeMenueColor(editemployeebg);// TODO add your handling code here:
    }//GEN-LAST:event_editEmloyeeBtnMouseClicked

    private void employeePaymentBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employeePaymentBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(employeepayment);
mainContent.repaint();
mainContent.revalidate();//
changeMenueColor(employeePaymentbg);// TODO add your handling code here:
loadEmployePayTable();
    }//GEN-LAST:event_employeePaymentBtnMouseClicked

    private void searchDetailsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchDetailsBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(searchDetails);
mainContent.repaint();
mainContent.revalidate();// 
changeMenueColor(searchDetailsbg);
 employeeDetailsPanel.setVisible(false);
 studentDetailsPanel.setVisible(false);
 roomDetailsPanel.setVisible(false);
    }//GEN-LAST:event_searchDetailsBtnMouseClicked

    private void messManagmentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_messManagmentMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(mess);
mainContent.repaint();
mainContent.revalidate();//     
changeMenueColor(messManagmentbg);
loadMessMenu();
String _day=(String)menuDay.getSelectedItem();
ArrayList<String> _result=managemess.getMessData(_day);
breakfast.setText(_result.get(1));
lunch.setText(_result.get(2));
dinner.setText(_result.get(3));
    }//GEN-LAST:event_messManagmentMouseClicked

    private void livingStudentsBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_livingStudentsBtnMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(studentsDisplay);
mainContent.repaint();
mainContent.revalidate();// 
changeMenueColor(displayStudents);
loaddisplayStudentsTable("All");
    }//GEN-LAST:event_livingStudentsBtnMouseClicked

    private void maxBedsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_maxBedsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_maxBedsActionPerformed

    private void addNewRoomBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addNewRoomBtnMouseClicked

    if(!roomNumber.getText().isEmpty() && !maxBeds.getText().isEmpty() && !roomFee.getText().isEmpty()){
           
                String activate;
                int _fee =0;
            
               
                 int _maxBeds=0;
                if(activateRoom.isSelected()){
                    activate="Active";
                }
                else
                {
                activate="Not Active";
                }
               
                try{
                _maxBeds=Integer.parseInt(maxBeds.getText());
                _fee = Integer.parseInt(roomFee.getText()); 
                 String _roomNumber=roomNumber.getText();
                room.addRoom(_roomNumber, activate,_maxBeds,_fee);
              loadroomsTable();
                }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Invalid Input");
            }
         roomNumberSearch.setBorder(defaultBorder);
         roomNumber.setBorder(defaultBorder);
         maxBeds.setBorder(defaultBorder);
         roomFee.setBorder(defaultBorder);
               
           
        }

        else{
        
         if(roomNumber.getText().isEmpty())
         {
             roomNumber.setBorder(new LineBorder(Color.red,1));
         }
          if(maxBeds.getText().isEmpty())
          {
              maxBeds.setBorder(new LineBorder(Color.red,1));
          }
           if(roomFee.getText().isEmpty())
           {
            roomFee.setBorder(new LineBorder(Color.red,1));
           }
        
           
        }       // TODO add your handling code here:
    }//GEN-LAST:event_addNewRoomBtnMouseClicked

    private void roomNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_roomNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_roomNumberActionPerformed

    private void roomNumberSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_roomNumberSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_roomNumberSearchActionPerformed

    private void searchRoomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchRoomMouseClicked

   if(!roomNumberSearch.getText().isEmpty()){
           
     String _roomNumber=roomNumberSearch.getText();
    ResultSet rs = room.searchRooms(_roomNumber);
     roomsTable.setModel(DbUtils.resultSetToTableModel(rs));          
   }

        else{
       roomNumberSearch.setBorder(new LineBorder(Color.red,1));
            
        } 
        // TODO add your handling code here:
    }//GEN-LAST:event_searchRoomMouseClicked

   
    private void roomNumberSearchInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_roomNumberSearchInputMethodTextChanged
        // TODO add your handling code here:
      
    }//GEN-LAST:event_roomNumberSearchInputMethodTextChanged

    private void showAllRoomsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_showAllRoomsMouseClicked
loadroomsTable();        // TODO add your handling code here:
    }//GEN-LAST:event_showAllRoomsMouseClicked

    private void findRoomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_findRoomMouseClicked
        if(!roomNumberFind.getText().isEmpty()){

            String _roomNumberFind=roomNumberFind.getText();
            ResultSet rs = room.searchRooms(_roomNumberFind);
            editRoomTable.setModel(DbUtils.resultSetToTableModel(rs));
            ArrayList<String> result=room.getRoomData(_roomNumberFind);

            if(result.size()>1)
            {
                
                  updateRoomNumber.setText(result.get(0));
            String _status=result.get(1);
            updatemaxBeds.setText(result.get(2));
            roomId.setText(result.get(3));
            roomId.setVisible(false);
            updatemaxBeds1.setText(result.get(4));
            if("Active".equals(_status)){
                updateactivateRoom.setSelected(true);
            }
            else{
                updateactivateRoom.setSelected(false);
            }

            }
           roomNumberFind.setBorder(defaultBorder);
        }

        else{
           roomNumberFind.setBorder(new LineBorder(Color.red,1));
        }
        // TODO add your handling code here:

    }//GEN-LAST:event_findRoomMouseClicked

    private void roomNumberFindActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_roomNumberFindActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_roomNumberFindActionPerformed

    private void deleteRoomBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deleteRoomBtnMouseClicked

     
        int _roomid=Integer.parseInt(roomId.getText());
        
        room.deleteRoom(_roomid);
    }//GEN-LAST:event_deleteRoomBtnMouseClicked

    private void updateRoomBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateRoomBtnMouseClicked
if(!updatemaxBeds.getText().isEmpty() && !updatemaxBeds1.getText().isEmpty() && !updateRoomNumber.getText().isEmpty() )
{
       String _roomNumber=updateRoomNumber.getText();
       
        int _roomid= Integer.parseInt(roomId.getText());
        int _maxBeds=Integer.parseInt(updatemaxBeds.getText());
        int _fee = Integer.parseInt(updatemaxBeds1.getText());
        String _status;

        if(updateactivateRoom.isSelected()){
            _status="Active";
        }
        else
        {
            _status="Not Active";
        }

        room.updateRoom(_roomNumber, _status, _maxBeds,_roomid,_fee);
  ResultSet rs = room.searchRooms(_roomNumber);
            editRoomTable.setModel(DbUtils.resultSetToTableModel(rs));
        // TODO add your handling code here:
}
else{
    
    if(updatemaxBeds.getText().isEmpty())
    {
         updatemaxBeds.setBorder(new LineBorder(Color.red,1));
    }
    else{
        
    }
    if(updatemaxBeds1.getText().isEmpty())
    {
          updatemaxBeds1.setBorder(new LineBorder(Color.red,1));
    }
    if(updateRoomNumber.getText().isEmpty() )
    {
          updateRoomNumber.setBorder(new LineBorder(Color.red,1));
    }
   
}
     
    }//GEN-LAST:event_updateRoomBtnMouseClicked

    private void updatemaxBedsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatemaxBedsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updatemaxBedsActionPerformed

    private void updateRoomNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateRoomNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updateRoomNumberActionPerformed

    private void studentNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_studentNameActionPerformed

    private void fatherNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fatherNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_fatherNameActionPerformed

    private void phoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_phoneNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_phoneNumberActionPerformed

    private void emailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emailActionPerformed

    private void cnicNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cnicNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cnicNumberActionPerformed

    private void addressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_addressActionPerformed

    private void collegeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_collegeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_collegeActionPerformed

    private void addStudentFormBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addStudentFormBtnMouseClicked
   if(!phoneNumber.getText().isEmpty() && !studentName.getText().isEmpty() && !fatherName.getText().isEmpty() && !email.getText().isEmpty()
           
           && !address.getText().isEmpty() && !college.getText().isEmpty() && !cnicNumber.getText().isEmpty()
           )
   {
          
        String _mobileNumber=phoneNumber.getText();
        String _studentName=studentName.getText();
        String _fatherName=fatherName.getText();
        String _email=email.getText();
        String _address=address.getText();
        String _college=college.getText();
        String _cnicNumber=cnicNumber.getText();
        String _roomNumber=(String)availableRooms.getSelectedItem();
        
        String _status; 
        if(studentStatus.isSelected()){
                    _status="Living";
                }
                else
                {
                _status="Not Living";
                }
        student.addStudent(_mobileNumber,_studentName,_fatherName,_email,_address,_college,_cnicNumber,_roomNumber,_status);
        clearAddstudentForm();
      
        loadavailableRooms();
phoneNumber.setBorder(defaultBorder); 
studentName.setBorder(defaultBorder); 
fatherName.setBorder(defaultBorder); 
email.setBorder(defaultBorder); 
address.setBorder(defaultBorder); 
college.setBorder(defaultBorder); 
cnicNumber.setBorder(defaultBorder); 
   }
   else{
       if(phoneNumber.getText().isEmpty())
       {
           phoneNumber.setBorder(new LineBorder(Color.red,1));
       }
       if(studentName.getText().isEmpty())
       {
           studentName.setBorder(new LineBorder(Color.red,1));
       }
       if(fatherName.getText().isEmpty())
       {
           fatherName.setBorder(new LineBorder(Color.red,1));
       }
       if(email.getText().isEmpty())
       {
           email.setBorder(new LineBorder(Color.red,1));
       }
       if(address.getText().isEmpty())
       {
           address.setBorder(new LineBorder(Color.red,1));
       }
       if(college.getText().isEmpty())
       {
           college.setBorder(new LineBorder(Color.red,1));
       }
       if(cnicNumber.getText().isEmpty())
       {
           cnicNumber.setBorder(new LineBorder(Color.red,1));
       }
       
   }
   
   
   
     
    }//GEN-LAST:event_addStudentFormBtnMouseClicked

    private void canelAddStudentBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canelAddStudentBtnMouseClicked
       
clearAddstudentForm();

loadavailableRooms();
 
        
    }//GEN-LAST:event_canelAddStudentBtnMouseClicked

    private void roomFeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_roomFeeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_roomFeeActionPerformed

    private void updatemaxBeds1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updatemaxBeds1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updatemaxBeds1ActionPerformed

    private void editstudentNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editstudentNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editstudentNameActionPerformed

    private void editfatherNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editfatherNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editfatherNameActionPerformed

    private void editphoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editphoneNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editphoneNumberActionPerformed

    private void editemailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editemailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editemailActionPerformed

    private void editcnicNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editcnicNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editcnicNumberActionPerformed

    private void editaddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editaddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editaddressActionPerformed

    private void editcollegeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editcollegeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editcollegeActionPerformed

    private void updateStudentFormBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateStudentFormBtnMouseClicked
        
        if((!editstudentName.getText().isEmpty()) &&(!editcnicNumber.getText().isEmpty()) &&(!editfatherName.getText().isEmpty())
                &&(!editaddress.getText().isEmpty())&&(!editphoneNumber.getText().isEmpty()) &&(!editcollege.getText().isEmpty())
                &&(!editemail.getText().isEmpty())){
           int _student=Integer.parseInt(editSearchStudent.getText());
         
        String _name=editstudentName.getText();
        String _cnic= editcnicNumber.getText();
        String _father= editfatherName.getText();
        String _address= editaddress.getText();
        String _number= editphoneNumber.getText();
        String _college= editcollege.getText();
        String _email= editemail.getText();
        String _room = (String)editavailableRooms.getSelectedItem();
        int _id=Integer.parseInt(studentid.getText());
        
         String _status; 
        if(studentStatusUpdate.isSelected()){
                    _status="Living";
                }
                else
                {
                _status="Not Living";
                }
        student.updateStudent(_number,_name, _father, _email, _address, _college, _cnic, _room, _status,_id);
        
        
        }
         else{
       if(editphoneNumber.getText().isEmpty())
       {
           editphoneNumber.setBorder(new LineBorder(Color.red,1));
       }
       if(editstudentName.getText().isEmpty())
       {
           editstudentName.setBorder(new LineBorder(Color.red,1));
       }
       if(editfatherName.getText().isEmpty())
       {
           editfatherName.setBorder(new LineBorder(Color.red,1));
       }
       if(editemail.getText().isEmpty())
       {
           editemail.setBorder(new LineBorder(Color.red,1));
       }
       if(editaddress.getText().isEmpty())
       {
           editaddress.setBorder(new LineBorder(Color.red,1));
       }
       if(editcollege.getText().isEmpty())
       {
           editcollege.setBorder(new LineBorder(Color.red,1));
       }
       if(editcnicNumber.getText().isEmpty())
       {
           editcnicNumber.setBorder(new LineBorder(Color.red,1));
       }
       
   }
    }//GEN-LAST:event_updateStudentFormBtnMouseClicked

    private void clearEditStudentFormMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clearEditStudentFormMouseClicked
 editSearchStudent.setText("");
         
         editstudentName.setText("");
        editcnicNumber.setText("");
        editfatherName.setText("");
        editaddress.setText("");
        editphoneNumber.setText("");
        editcollege.setText("");
        editemail.setText("");
        studentid.setText("");
        
       
     
        
        
               // TODO add your handling code here:
    }//GEN-LAST:event_clearEditStudentFormMouseClicked

    private void employeePhoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeePhoneNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeePhoneNumberActionPerformed

    private void employeeCnicNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeCnicNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeCnicNumberActionPerformed

    private void employeeAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeAddressActionPerformed

    private void addEmployeeFormBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addEmployeeFormBtnMouseClicked
        
        if(!employeeName.getText().isEmpty() && !employeePhoneNumber.getText().isEmpty() && !employeeAddress.getText().isEmpty()
               && !employeeCnicNumber.getText().isEmpty()
                )
        {
        String _mobileNumber=employeePhoneNumber.getText();
        String _employeeName=employeeName.getText();
        String _address=employeeAddress.getText();
        String _cnicNumber=employeeCnicNumber.getText();
        String _designation=(String)designation.getSelectedItem();
        String _status=(String)employeeStatus.getSelectedItem();
        
        employee.addEmployee(_mobileNumber,_employeeName,_address,_cnicNumber,_designation,_status);
        clearAddemployeeForm();
        loadEmployeeTable();
        employeeName.setBorder(defaultBorder);
        employeePhoneNumber.setBorder(defaultBorder);
        employeeCnicNumber.setBorder(defaultBorder);
        employeeAddress.setBorder(defaultBorder);
        }
        else{
             if(employeeName.getText().isEmpty())
             {
                 employeeName.setBorder(new LineBorder(Color.red,1));
             }
              if(employeePhoneNumber.getText().isEmpty())
              {
               employeePhoneNumber.setBorder(new LineBorder(Color.red,1));   
              }
             if(employeeAddress.getText().isEmpty())
             {
                   employeeAddress.setBorder(new LineBorder(Color.red,1));
             }
              if(employeeCnicNumber.getText().isEmpty())
             {
              employeeCnicNumber.setBorder(new LineBorder(Color.red,1));   
             }
        }
       
    }//GEN-LAST:event_addEmployeeFormBtnMouseClicked

    private void canelAdsEmployeeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canelAdsEmployeeBtnMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_canelAdsEmployeeBtnMouseClicked

    private void updateEmployeePhoneNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateEmployeePhoneNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updateEmployeePhoneNumberActionPerformed

    private void updateEmployeeCnicNumberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateEmployeeCnicNumberActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updateEmployeeCnicNumberActionPerformed

    private void updateEmployeeAddressActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateEmployeeAddressActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updateEmployeeAddressActionPerformed

    private void clearUpdateemployeeFormMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clearUpdateemployeeFormMouseClicked
        // TODO add your handling code here:
        
        clearUpdateemployeeForm();
      
    }//GEN-LAST:event_clearUpdateemployeeFormMouseClicked

    private void canelUpdateEmployeeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_canelUpdateEmployeeBtnMouseClicked
        // TODO add your handling code here:
        
        int _id=Integer.parseInt(employeeId.getText());

        
        employee.deleteEmployee(_id);
            
    }//GEN-LAST:event_canelUpdateEmployeeBtnMouseClicked

    private void employeeSearchInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_employeeSearchInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeSearchInputMethodTextChanged

    private void employeeSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeSearchActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_employeeSearchActionPerformed

    private void searchEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchEmployeeMouseClicked
        // TODO add your handling code here:
        
        int _id=Integer.parseInt(employeeSearch.getText());
        if(!employeeSearch.getText().isEmpty()){
        employeeSearch.setBorder(defaultBorder);
        ArrayList<String> result =employee.getEmployeeData(_id);
        updateEmployeeName.setText(result.get(0));
        updateEmployeeCnicNumber.setText(result.get(1));
        updateEmployeePhoneNumber.setText(result.get(2));
        updateEmployeeAddress.setText(result.get(4));
        updateemployeeStatus.setSelectedItem(result.get(3));
        updateDesignation.setSelectedItem(result.get(5));
        employeeId.setText(result.get(6));
        employeeId.setVisible(false);
  
    ResultSet rs = employee.searchEmployee(_id);
     employeeTable.setModel(DbUtils.resultSetToTableModel(rs));          
   }

        else{
            employeeSearch.setBorder(new LineBorder(Color.red,1));
        } 
    }//GEN-LAST:event_searchEmployeeMouseClicked

    private void studentSearchInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_studentSearchInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_studentSearchInputMethodTextChanged

    private void studentSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_studentSearchActionPerformed

    private void searchStudentFeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchStudentFeeMouseClicked
        // TODO add your handling code here:
         
        if(!studentSearch.getText().isEmpty()){
            int _id=Integer.parseInt(studentSearch.getText());
             studentSearch.setBorder(defaultBorder);
        ArrayList<String> result =student.getStudentData(_id);
        //studentSearch.setText(result.get(0));
        studentNameFee.setText(result.get(0));
        studentNameFee.setEditable(false);
        studentCnicFee.setText(result.get(1));
        studentCnicFee.setEditable(false);
        StudentRoomIdFee.setText(result.get(7));
        StudentRoomIdFee.setEditable(false);
        feestudentid.setText(result.get(8));
        feestudentid.setVisible(false);     ResultSet rs = student.fee.searchFeeChallan(_id);
     studentFeeTable.setModel(DbUtils.resultSetToTableModel(rs));
     
        }
        else{
            studentSearch.setBorder(new LineBorder(Color.red,1));
        }
        
   
     //clearStudentFee();
    }//GEN-LAST:event_searchStudentFeeMouseClicked

    private void studentCnicFeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentCnicFeeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_studentCnicFeeActionPerformed

    private void challanExpiryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_challanExpiryActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_challanExpiryActionPerformed

    private void StudentRoomIdFeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StudentRoomIdFeeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_StudentRoomIdFeeActionPerformed

    private void studentNameFeeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentNameFeeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_studentNameFeeActionPerformed

    private void extraAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_extraAmountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_extraAmountActionPerformed

    private void sendChallanToAllMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sendChallanToAllMouseClicked
        // TODO add your handling code here:

      student.fee.sendFeetoAll();
        //loadEmployeeTable();
    }//GEN-LAST:event_sendChallanToAllMouseClicked

    private void cancelFeeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelFeeBtnMouseClicked
        // TODO add your handling code here:
        clearStudentFee();
    }//GEN-LAST:event_cancelFeeBtnMouseClicked

    private void sendFeeBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sendFeeBtnMouseClicked

        challanExpiry.setBorder(defaultBorder);
 extraAmount.setBorder(defaultBorder);
 
 if(!challanExpiry.getText().isEmpty() && !extraAmount.getText().isEmpty())
 {
      int _id=Integer.parseInt(feestudentid.getText());
        String _month=(String) feeMonth.getSelectedItem();
        String _expDays=challanExpiry.getText();
        int _extraCharges=Integer.parseInt(extraAmount.getText());
       if(student.fee.issueChallan(_id, _month, _expDays, _extraCharges)==true){
                ResultSet rs = student.fee.searchFeeChallan(_id);
     studentFeeTable.setModel(DbUtils.resultSetToTableModel(rs));
      JOptionPane.showMessageDialog(null, "Fee Challan Has Been Sended");
       }
       

 }
 else{
     if(challanExpiry.getText().isEmpty())
     {
             challanExpiry.setBorder(new LineBorder(Color.red,1));
 
     }
      if(extraAmount.getText().isEmpty())
      {
          extraAmount.setBorder(new LineBorder(Color.red,1));
      }
 }
       
       

        // TODO add your handling code here:
    }//GEN-LAST:event_sendFeeBtnMouseClicked

    private void editSearchStudentInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_editSearchStudentInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_editSearchStudentInputMethodTextChanged

    private void editSearchStudentActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSearchStudentActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_editSearchStudentActionPerformed

    private void employeepayamountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeepayamountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeepayamountActionPerformed

    private void cancelFeeBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cancelFeeBtn1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_cancelFeeBtn1MouseClicked

    private void sendpayMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sendpayMouseClicked
if(!employeeidpay.getText().isEmpty() && !employeepayamount.getText().isEmpty())
{
    employeepayamount.setBorder(defaultBorder);
    employeeidpay.setBorder(defaultBorder);
       int _id =Integer.parseInt(employeeidpay.getText().trim());
        int _amount =Integer.parseInt(employeepayamount.getText().trim());
        employee.pay.payPayment(_id, _amount);
        loadEmployePayTable(); 
}
else{
    if(employeeidpay.getText().isEmpty()){
     employeeidpay.setBorder(new LineBorder(Color.red,1));   
    }
    if(employeepayamount.getText().isEmpty())
    {
        employeepayamount.setBorder(new LineBorder(Color.red,1)); 
    }
    
}
    
    }//GEN-LAST:event_sendpayMouseClicked

    private void employeeidpayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeidpayActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeidpayActionPerformed

    private void breakfastActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_breakfastActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_breakfastActionPerformed

    private void lunchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lunchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_lunchActionPerformed

    private void dinnerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_dinnerActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_dinnerActionPerformed

    private void updateMenuMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateMenuMouseClicked
String _day=(String)menuDay.getSelectedItem();
String _breakfast=breakfast.getText();
String _lunch=lunch.getText();
String _dinner=dinner.getText();
managemess.updateMess(_day,_breakfast,_lunch,_dinner);  
loadMessMenu();// TODO add your handling code here:
    }//GEN-LAST:event_updateMenuMouseClicked

    private void menuDayItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_menuDayItemStateChanged

String _day=(String)menuDay.getSelectedItem();
ArrayList<String> _result=managemess.getMessData(_day);
breakfast.setText(_result.get(1));
lunch.setText(_result.get(2));
dinner.setText(_result.get(3));
        // TODO add your handling code here:
    }//GEN-LAST:event_menuDayItemStateChanged

    private void searchStudentDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchStudentDataActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchStudentDataActionPerformed

    private void searchStudentDatabtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchStudentDatabtnMouseClicked
            int _id=Integer.parseInt(searchStudentData.getText());
            ResultSet rs=student.searchStudent(_id);
            displayStudentsTable.setModel(DbUtils.resultSetToTableModel(rs));
    }//GEN-LAST:event_searchStudentDatabtnMouseClicked

    private void livingStatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_livingStatusItemStateChanged

        String _status=(String)livingStatus.getSelectedItem();
loaddisplayStudentsTable( _status);
    }//GEN-LAST:event_livingStatusItemStateChanged

    private void searchDetailsIdActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchDetailsIdActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchDetailsIdActionPerformed

    private void searchDetailsbtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchDetailsbtnMouseClicked
if(!searchDetailsId.getText().isEmpty())
{
    searchDetailsId.setBorder(defaultBorder);
String _selected=(String)searchDetailsby.getSelectedItem();
String _id=searchDetailsId.getText();
if(_selected=="Student"){
   
    ResultSet rs = student.searchStudent(Integer.parseInt(_id));
    studentDetailTable.setModel(DbUtils.resultSetToTableModel(rs));
    
    ResultSet rs2 = student.fee.searchFeeChallan(Integer.parseInt(_id));
    studentFeeDetailsTable.setModel(DbUtils.resultSetToTableModel(rs2));
     showDetailsPanel(studentDetailsPanel);
    
}
else if(_selected=="Employee")
{
    ResultSet rs = employee.searchEmployee(Integer.parseInt(_id));
    employeDetailsTable.setModel(DbUtils.resultSetToTableModel(rs));
    
    ResultSet rs2 = employee.pay.paymentHistory(Integer.parseInt(_id));
    employeePaymentDetailsTable.setModel(DbUtils.resultSetToTableModel(rs2));
     showDetailsPanel(employeeDetailsPanel);
     
}
else if(_selected=="Room")
{
    ResultSet rs = room.searchRooms(_id);
    roomDetailsTable.setModel(DbUtils.resultSetToTableModel(rs));
     ResultSet rs2 = room.displayRoomMembers(_id);
    roomMembersDetails.setModel(DbUtils.resultSetToTableModel(rs2));
     showDetailsPanel(roomDetailsPanel);
}  


}
else{
    searchDetailsId.setBorder(new LineBorder(Color.red,1));
}
// TODO add your handling code here:
    }//GEN-LAST:event_searchDetailsbtnMouseClicked

    private void searchDetailsbyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_searchDetailsbyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_searchDetailsbyActionPerformed

    private void loginEmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_loginEmailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_loginEmailActionPerformed

    private void updateEmployeeFormBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateEmployeeFormBtn1MouseClicked

        String _email= loginEmail.getText();
        String _password=loginPassword.getText();
if(login(_email,_password)==false)
{
    wrongEmailorPass.setText("Wrong Email or Password");
}
    }//GEN-LAST:event_updateEmployeeFormBtn1MouseClicked

    private void logoutBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_logoutBtnMouseClicked
logout();
loginPassword.setText("");
 wrongEmailorPass.setText("");

    }//GEN-LAST:event_logoutBtnMouseClicked

    private void updateEmployeeFormBtn2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_updateEmployeeFormBtn2MouseClicked
  
         if(!updateEmployeeName.getText().isEmpty() && !updateEmployeePhoneNumber.getText().isEmpty() && !updateEmployeeAddress.getText().isEmpty()
               && !updateEmployeeCnicNumber.getText().isEmpty()
                )
        {
        String _name=updateEmployeeName.getText();
        String _cnic=updateEmployeeCnicNumber.getText();
        String _mobileNumber=updateEmployeePhoneNumber.getText();
        String _address=updateEmployeeAddress.getText();
        String _status=(String)updateemployeeStatus.getSelectedItem();
        String _designation=(String)updateDesignation.getSelectedItem();
        int _id=Integer.parseInt(employeeId.getText());

        employee.updateEmployee(_mobileNumber, _name,  _address, _cnic, _designation, _status,_id);
        ResultSet rs = employee.searchEmployee(_id);  
        employeeTable.setModel(DbUtils.resultSetToTableModel(rs));
             
         updateEmployeeName.setBorder(defaultBorder);
        updateEmployeePhoneNumber.setBorder(defaultBorder);
        updateEmployeeCnicNumber.setBorder(defaultBorder);
        updateEmployeeAddress.setBorder(defaultBorder);
        }
        else{
             if(updateEmployeeName.getText().isEmpty())
             {
                 updateEmployeeName.setBorder(new LineBorder(Color.red,1));
             }
              if(updateEmployeePhoneNumber.getText().isEmpty())
              {
               updateEmployeePhoneNumber.setBorder(new LineBorder(Color.red,1));   
              }
             if(updateEmployeeAddress.getText().isEmpty())
             {
                   updateEmployeeAddress.setBorder(new LineBorder(Color.red,1));
             }
              if(updateEmployeeCnicNumber.getText().isEmpty())
             {
              updateEmployeeCnicNumber.setBorder(new LineBorder(Color.red,1));   
             }
        }
    }//GEN-LAST:event_updateEmployeeFormBtn2MouseClicked

    private void studentStatusActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentStatusActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_studentStatusActionPerformed

    private void studentStatusUpdateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studentStatusUpdateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_studentStatusUpdateActionPerformed

    private void clearEditStudentForm1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_clearEditStudentForm1MouseClicked

        int _id=Integer.parseInt(studentid.getText());
        student.deleteStudent(_id);        // TODO add your handling code here:
    }//GEN-LAST:event_clearEditStudentForm1MouseClicked

    private void addRoomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addRoomMouseClicked
 
         roomNumberSearch.setBorder(defaultBorder);
         roomNumber.setBorder(defaultBorder);
         maxBeds.setBorder(defaultBorder);
         roomFee.setBorder(defaultBorder);
        
// TODO add your handling code here:
    }//GEN-LAST:event_addRoomMouseClicked

    private void editRoomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editRoomMouseClicked
roomNumberFind.setBorder(defaultBorder); 
updateRoomNumber.setBorder(defaultBorder); 
updatemaxBeds.setBorder(defaultBorder); 
updatemaxBeds1.setBorder(defaultBorder); 
    }//GEN-LAST:event_editRoomMouseClicked

    private void addStudentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addStudentMouseClicked
phoneNumber.setBorder(defaultBorder); 
studentName.setBorder(defaultBorder); 
fatherName.setBorder(defaultBorder); 
email.setBorder(defaultBorder); 
address.setBorder(defaultBorder); 
college.setBorder(defaultBorder); 
cnicNumber.setBorder(defaultBorder);       // TODO add your handling code here:
    }//GEN-LAST:event_addStudentMouseClicked

    private void searchStudentEditMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchStudentEditMouseClicked
      

      if(!editSearchStudent.getText().isEmpty()){
           int _student=Integer.parseInt(editSearchStudent.getText());
           ArrayList<String> _result= student.getStudentData(_student);
         editstudentName.setText(_result.get(0));
         editcnicNumber.setText(_result.get(1));
         editfatherName.setText(_result.get(2));
         editaddress.setText(_result.get(4));
         editphoneNumber.setText(_result.get(3));
         editcollege.setText(_result.get(6));
         editemail.setText(_result.get(5));
         editavailableRooms.removeAllItems();
         studentid.setText(_result.get(8));
         studentid.setVisible(false);
         editavailableRooms.addItem((_result.get(7)));  
ArrayList<String> _rooms= room.getavailableRooms();

for(int i=0;i<_rooms.size();i++){
    
   editavailableRooms.addItem(_rooms.get(i));
}
        
           
           
      }     
      
      else{
          editSearchStudent.setBorder(new LineBorder(Color.red,1));
      }
// TODO add your handling code here:
    }//GEN-LAST:event_searchStudentEditMouseClicked

    private void editStudentsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editStudentsMouseClicked
         editSearchStudent.setBorder(defaultBorder);  
         editstudentName.setBorder(defaultBorder); 
         editcnicNumber.setBorder(defaultBorder); 
         editfatherName.setBorder(defaultBorder); 
         editaddress.setBorder(defaultBorder); 
         editphoneNumber.setBorder(defaultBorder); 
         editcollege.setBorder(defaultBorder); 
         editemail.setBorder(defaultBorder); 
        
         
   
    }//GEN-LAST:event_editStudentsMouseClicked

    private void studentsFeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_studentsFeeMouseClicked
 studentSearch.setBorder(defaultBorder);
 challanExpiry.setBorder(defaultBorder);
 extraAmount.setBorder(defaultBorder);
    }//GEN-LAST:event_studentsFeeMouseClicked

    private void addEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_addEmployeeMouseClicked
employeeName.setBorder(defaultBorder);
        employeePhoneNumber.setBorder(defaultBorder);
        employeeCnicNumber.setBorder(defaultBorder);
        employeeAddress.setBorder(defaultBorder);        // TODO add your handling code here:
    }//GEN-LAST:event_addEmployeeMouseClicked

    private void editEmployeeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_editEmployeeMouseClicked
  updateEmployeeName.setBorder(defaultBorder);
        updateEmployeePhoneNumber.setBorder(defaultBorder);
        updateEmployeeCnicNumber.setBorder(defaultBorder);
        updateEmployeeAddress.setBorder(defaultBorder);        // TODO add your handling code here:
    }//GEN-LAST:event_editEmployeeMouseClicked

    private void updateEmployeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateEmployeeNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_updateEmployeeNameActionPerformed

    private void employeeNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_employeeNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_employeeNameActionPerformed

    private void employeepaymentMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_employeepaymentMouseClicked
  employeepayamount.setBorder(defaultBorder);
    employeeidpay.setBorder(defaultBorder);        // TODO add your handling code here:
    }//GEN-LAST:event_employeepaymentMouseClicked

    private void searchDetailsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_searchDetailsMouseClicked
 searchDetailsId.setBorder(defaultBorder);        // TODO add your handling code here:
    }//GEN-LAST:event_searchDetailsMouseClicked

    private void feeReportMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_feeReportMouseClicked
mainContent.removeAll();
mainContent.repaint();
mainContent.revalidate();

mainContent.add(feeReports);
mainContent.repaint();
mainContent.revalidate();// 
changeMenueColor(feeReportbg);
loadFeeReportTable("All");
    }//GEN-LAST:event_feeReportMouseClicked

    private void feeStatusItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_feeStatusItemStateChanged

        String _status=(String)feeStatus.getSelectedItem();
loadFeeReportTable(_status);
        // TODO add your handling code here:
    }//GEN-LAST:event_feeStatusItemStateChanged

    /**
     * @param args the command line arguments
     */
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(admin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
         

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new admin().setVisible(true);
             
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel AdminPanel;
    private javax.swing.JTextField StudentRoomIdFee;
    private javax.swing.JCheckBox activateRoom;
    private javax.swing.JLabel addEmpeloyeBtn;
    private javax.swing.JPanel addEmployee;
    private javax.swing.JLabel addEmployeeFormBtn;
    private javax.swing.JPanel addEmployeebg;
    private javax.swing.JLabel addNewRoomBtn;
    private javax.swing.JPanel addRoom;
    private javax.swing.JPanel addRoomsBg;
    private javax.swing.JLabel addRoomsBtn;
    private javax.swing.JPanel addStudent;
    private javax.swing.JLabel addStudentBtn;
    private javax.swing.JLabel addStudentFormBtn;
    private javax.swing.JPanel addStudentbg;
    private javax.swing.JTextField address;
    private javax.swing.JComboBox<String> availableRooms;
    private javax.swing.JTextField breakfast;
    private javax.swing.JLabel cancelFeeBtn;
    private javax.swing.JLabel cancelFeeBtn1;
    private javax.swing.JLabel canelAddStudentBtn;
    private javax.swing.JLabel canelAdsEmployeeBtn;
    private javax.swing.JLabel canelUpdateEmployeeBtn;
    private javax.swing.JTextField challanExpiry;
    private javax.swing.JLabel clearEditStudentForm;
    private javax.swing.JLabel clearEditStudentForm1;
    private javax.swing.JLabel clearUpdateemployeeForm;
    private javax.swing.JTextField cnicNumber;
    private javax.swing.JTextField college;
    private javax.swing.JLabel deleteRoomBtn;
    private javax.swing.JComboBox<String> designation;
    private javax.swing.JTextField dinner;
    private javax.swing.JTable displayEmployee;
    private javax.swing.JPanel displayStudents;
    private javax.swing.JTable displayStudentsTable;
    private javax.swing.JLabel editEmloyeeBtn;
    private javax.swing.JPanel editEmployee;
    private javax.swing.JPanel editRoom;
    private javax.swing.JTable editRoomTable;
    private javax.swing.JPanel editRoombg;
    private javax.swing.JLabel editRoomsBtn;
    private javax.swing.JTextField editSearchStudent;
    private javax.swing.JLabel editStudentBtn;
    private javax.swing.JPanel editStudentbg;
    private javax.swing.JPanel editStudents;
    private javax.swing.JTextField editaddress;
    private javax.swing.JComboBox<String> editavailableRooms;
    private javax.swing.JTextField editcnicNumber;
    private javax.swing.JTextField editcollege;
    private javax.swing.JTextField editemail;
    private javax.swing.JPanel editemployeebg;
    private javax.swing.JTextField editfatherName;
    private javax.swing.JTextField editphoneNumber;
    private javax.swing.JTextField editstudentName;
    private javax.swing.JTextField email;
    private javax.swing.JTable employeDetailsTable;
    private javax.swing.JTextField employeeAddress;
    private javax.swing.JTextField employeeCnicNumber;
    private javax.swing.JPanel employeeDetailsPanel;
    private javax.swing.JLabel employeeId;
    private javax.swing.JTextField employeeName;
    private javax.swing.JLabel employeeNum;
    private javax.swing.JTable employeePayTable;
    private javax.swing.JLabel employeePaymentBtn;
    private javax.swing.JTable employeePaymentDetailsTable;
    private javax.swing.JPanel employeePaymentbg;
    private javax.swing.JTextField employeePhoneNumber;
    private javax.swing.JTextField employeeSearch;
    private javax.swing.JComboBox<String> employeeStatus;
    private javax.swing.JTable employeeTable;
    private javax.swing.JTextField employeeidpay;
    private javax.swing.JTextField employeepayamount;
    private javax.swing.JPanel employeepayment;
    private javax.swing.JTextField extraAmount;
    private javax.swing.JTextField fatherName;
    private javax.swing.JComboBox<String> feeMonth;
    private javax.swing.JLabel feeReport;
    private javax.swing.JTable feeReportTable;
    private javax.swing.JPanel feeReportbg;
    private javax.swing.JPanel feeReports;
    private javax.swing.JComboBox<String> feeStatus;
    private javax.swing.JLabel feestudentid;
    private javax.swing.JLabel feestudentid1;
    private javax.swing.JLabel findRoom;
    private javax.swing.JLabel homeBtn;
    private javax.swing.JPanel homeBtnbg;
    private javax.swing.JPanel homePage;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel39;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel53;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel84;
    private javax.swing.JLabel jLabel85;
    private javax.swing.JLabel jLabel86;
    private javax.swing.JLabel jLabel87;
    private javax.swing.JLabel jLabel88;
    private javax.swing.JLabel jLabel89;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel90;
    private javax.swing.JLabel jLabel91;
    private javax.swing.JLabel jLabel93;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel24;
    private javax.swing.JPanel jPanel25;
    private javax.swing.JPanel jPanel26;
    private javax.swing.JPanel jPanel27;
    private javax.swing.JPanel jPanel28;
    private javax.swing.JPanel jPanel29;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel30;
    private javax.swing.JPanel jPanel31;
    private javax.swing.JPanel jPanel32;
    private javax.swing.JPanel jPanel33;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel35;
    private javax.swing.JPanel jPanel36;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane11;
    private javax.swing.JScrollPane jScrollPane12;
    private javax.swing.JScrollPane jScrollPane13;
    private javax.swing.JScrollPane jScrollPane14;
    private javax.swing.JScrollPane jScrollPane15;
    private javax.swing.JScrollPane jScrollPane16;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    private keeptoo.KGradientPanel kGradientPanel1;
    private javax.swing.JComboBox<String> livingStatus;
    private javax.swing.JLabel livingStudentsBtn;
    private javax.swing.JTextField loginEmail;
    private javax.swing.JPanel loginPanel;
    private javax.swing.JPasswordField loginPassword;
    private javax.swing.JLabel logo;
    private javax.swing.JPanel logout;
    private javax.swing.JLabel logoutBtn;
    private javax.swing.JTextField lunch;
    private javax.swing.JPanel mainContent;
    private javax.swing.JTextField maxBeds;
    private javax.swing.JComboBox<String> menuDay;
    private javax.swing.JTable menuTable;
    private javax.swing.JPanel mess;
    private javax.swing.JLabel messManagment;
    private javax.swing.JPanel messManagmentbg;
    private javax.swing.JPanel navigationBar;
    private javax.swing.JTextField phoneNumber;
    private javax.swing.JPanel roomDetailsPanel;
    private javax.swing.JTable roomDetailsTable;
    private javax.swing.JTextField roomFee;
    private javax.swing.JLabel roomId;
    private javax.swing.JTable roomMembersDetails;
    private javax.swing.JLabel roomNum;
    private javax.swing.JTextField roomNumber;
    private javax.swing.JTextField roomNumberFind;
    private javax.swing.JTextField roomNumberSearch;
    private javax.swing.JTable roomsTable;
    private javax.swing.JPanel searchDetails;
    private javax.swing.JLabel searchDetailsBtn;
    private javax.swing.JTextField searchDetailsId;
    private javax.swing.JPanel searchDetailsbg;
    private javax.swing.JLabel searchDetailsbtn;
    private javax.swing.JComboBox<String> searchDetailsby;
    private javax.swing.JLabel searchEmployee;
    private javax.swing.JLabel searchRoom;
    private javax.swing.JTextField searchStudentData;
    private javax.swing.JLabel searchStudentDatabtn;
    private javax.swing.JLabel searchStudentEdit;
    private javax.swing.JLabel searchStudentFee;
    private javax.swing.JLabel sendChallanToAll;
    private javax.swing.JLabel sendFeeBtn;
    private javax.swing.JLabel sendpay;
    private javax.swing.JLabel showAllRooms;
    private javax.swing.JTextField studentCnicFee;
    private javax.swing.JTable studentDetailTable;
    private javax.swing.JPanel studentDetailsPanel;
    private javax.swing.JLabel studentFeeBtn;
    private javax.swing.JTable studentFeeDetailsTable;
    private javax.swing.JTable studentFeeTable;
    private javax.swing.JPanel studentFeebg;
    private javax.swing.JTextField studentName;
    private javax.swing.JTextField studentNameFee;
    private javax.swing.JTextField studentSearch;
    private javax.swing.JCheckBox studentStatus;
    private javax.swing.JCheckBox studentStatusUpdate;
    private javax.swing.JLabel studentid;
    private javax.swing.JPanel studentsDisplay;
    private javax.swing.JPanel studentsFee;
    private javax.swing.JLabel studentsNum;
    private javax.swing.JComboBox<String> updateDesignation;
    private javax.swing.JTextField updateEmployeeAddress;
    private javax.swing.JTextField updateEmployeeCnicNumber;
    private javax.swing.JLabel updateEmployeeFormBtn1;
    private javax.swing.JLabel updateEmployeeFormBtn2;
    private javax.swing.JTextField updateEmployeeName;
    private javax.swing.JTextField updateEmployeePhoneNumber;
    private javax.swing.JLabel updateMenu;
    private javax.swing.JLabel updateRoomBtn;
    private javax.swing.JTextField updateRoomNumber;
    private javax.swing.JLabel updateStudentFormBtn;
    private javax.swing.JCheckBox updateactivateRoom;
    private javax.swing.JComboBox<String> updateemployeeStatus;
    private javax.swing.JTextField updatemaxBeds;
    private javax.swing.JTextField updatemaxBeds1;
    private javax.swing.JLabel wrongEmailorPass;
    // End of variables declaration//GEN-END:variables
}
