/**
 *
 * @author talha
 */

package hostelmanagment;

import modules.ConnectionProvider;
import modules.PasswordGenerator;
import modules.SendMail;
import java.sql.*;
import java.util.ArrayList;
import javax.swing.JOptionPane;


public class Employee {
   
    private int employeeId;
    private String employeeName; 
    private String phoneNumber;
    private String email;
    private String cnicNumber;
    private String address;
    private String designation;
    private String status;
    Pay pay= new Pay();
   Connection con=ConnectionProvider.getCon();

public void addEmployee(String _mobileNumber,
        String _employeeName,
        String _address,
        String _cnicNumber,
        String _designation,
        String _status){
    
    
   this.employeeName=_employeeName;
   this.phoneNumber=_mobileNumber;
   this.address=_address;
   this.cnicNumber=_cnicNumber;
   this.designation=_designation;
   this.status="Working";
    try{
       PreparedStatement ps=con.prepareStatement("insert into employee (mobileNo,name,address,cnic,designation,status) values(?,?,?,?,?,?)");
            ps.setString(1, phoneNumber);
            ps.setString(2, employeeName);
            ps.setString(3, address);
            ps.setString(4, cnicNumber);
            ps.setString(5, designation);
            ps.setString(6, status);
            ps.executeUpdate();
            
             JOptionPane.showMessageDialog(null, "Employee Added Successfuly");
      
    
    }
    catch(Exception e)
    {
         JOptionPane.showMessageDialog(null, e);

          JOptionPane.showMessageDialog(null, "Employee Is Already Present In DataBase");
    }
    
    
    
}
public void updateEmployee(String _mobileNumber,
        
        String _employeeName,
        String _address,
        String _cnicNumber,
        String _designation,
        String _status,
        int _id)
{    
   this.employeeName=_employeeName;
   this.phoneNumber=_mobileNumber;
   this.address=_address;
   this.cnicNumber=_cnicNumber;
   this.designation=_designation;
   this.status=_status;
   this.employeeId=_id;
    
    try{
       PreparedStatement ps=con.prepareStatement("update employee Set mobileNo=?,name=?,address=?,cnic=?,designation=?,status=? where id=?");
            ps.setString(1, phoneNumber);
            ps.setString(2, employeeName);
            ps.setString(3, address);
            ps.setString(4, cnicNumber);
            ps.setString(5, designation);
            ps.setString(6, status);
            ps.setInt(7, employeeId);
            ps.executeUpdate();
              JOptionPane.showMessageDialog(null, "Employee Data Updated Successfuly");
      
    }
    catch(Exception e)
    { JOptionPane.showMessageDialog(null, e);
         
          JOptionPane.showMessageDialog(null, "Error! Can't Update Employee Data");
    }
     
    
    
}
public  ResultSet displayEmployees()    {
  ResultSet rs=null;
     try{
     
        Statement st=con.createStatement();
        rs=st.executeQuery("Select id as 'Employee ID', name AS 'Name' ,cnic as 'CNIC', mobileNo as 'Phone',address as 'Address' ,designation As 'Designation',status as 'Status' from employee");
        
      
        return rs;
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }
 return rs;
}
public  ResultSet searchEmployee(int _id)    {
  ResultSet rs=null;
  this.employeeId=_id;
     try{
      
        Statement st=con.createStatement();
                  rs=st.executeQuery("Select id as 'Employee ID', name AS 'Name' ,cnic as 'CNIC', mobileNo as 'Phone',address as 'Address' ,designation As 'Designation',status as 'Status' from employee where id='"+employeeId+"'");

    
         return rs;
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }
 return rs;
}
public  ArrayList<String> getEmployeeData(int _id)    {
  ArrayList<String> result=new ArrayList<String>();
  this.employeeId=_id;
     try{
      
        Statement st=con.createStatement();
      ResultSet rs=st.executeQuery("Select * from employee where id='"+employeeId+"'");
    if(rs.next()){
        
        result.add(rs.getString("name"));
        result.add(rs.getString("cnic"));
        result.add(rs.getString("mobileNo"));
        result.add(rs.getString("status"));
        result.add(rs.getString("address"));
        result.add(rs.getString("designation"));
        result.add(rs.getString("id"));
        
        
    }
    else{
        JOptionPane.showMessageDialog(null, "Employee Not Found");
    }
         
    }
    catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    
    }

     return result;
}
public void deleteEmployee(int _id){
         this.employeeId=_id;
           try{
        
       java.sql.Statement st=con.createStatement();
       ResultSet rs=st.executeQuery("Select * from employee where id='"+employeeId+"'");
        
      
          
         if(rs.next()){
             
            PreparedStatement ps=con.prepareStatement("Delete from employee where id=?");
            ps.setInt(1, employeeId);
            ps.executeUpdate();
            JOptionPane.showMessageDialog(null, " Employee Deleted Successfully");
         }
         else{
              JOptionPane.showMessageDialog(null, " Employee Not Found");
         }
           
            
          
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
            JOptionPane.showMessageDialog(null, "Employee Not Found");
        }
    }
  
  
 
}
