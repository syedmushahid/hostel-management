/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package modules;

/**
 *
 * @author syedm
 */
public class PasswordGenerator {
    
    
      // function to generate a random string of length n
   public static String getAlphaNumericString(int n)
    {
  
    
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                                    + "0123456789"
                                    + "abcdefghijklmnopqrstuvxyz";
  
   
        StringBuilder sb = new StringBuilder(n);
  
        for (int i = 0; i < n; i++) {
  
        
            int index
                = (int)(AlphaNumericString.length()
                        * Math.random());
  
          
            sb.append(AlphaNumericString
                          .charAt(index));
        }
  
        return sb.toString();
    }
    
}
