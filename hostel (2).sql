-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 24, 2022 at 10:34 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hostel`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `email`, `password`) VALUES
(0, 'syedmk600@gmail.com', '1618814');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(10) NOT NULL,
  `mobileNo` varchar(40) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `cnic` varchar(20) NOT NULL,
  `designation` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `mobileNo`, `name`, `address`, `cnic`, `designation`, `status`) VALUES
(11, '03987626262', 'Akbar Khan ', 'Rawalpindi', '78232-32323-23239-23', 'Cook', 'Working'),
(12, '03456762543', 'Kazim', 'Islamabad', '78232-23232-2323', 'Security', 'Working');

-- --------------------------------------------------------

--
-- Table structure for table `fee`
--

CREATE TABLE `fee` (
  `id` int(11) NOT NULL,
  `studentId` varchar(20) NOT NULL,
  `status` varchar(100) NOT NULL,
  `month` varchar(100) NOT NULL,
  `expDate` date DEFAULT NULL,
  `extraCharges` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee`
--

INSERT INTO `fee` (`id`, `studentId`, `status`, `month`, `expDate`, `extraCharges`) VALUES
(96, '48', 'Paid', 'January', '2022-01-30', 0),
(97, '48', 'Un Paid', 'January', '2022-01-30', 0);

-- --------------------------------------------------------

--
-- Table structure for table `mess`
--

CREATE TABLE `mess` (
  `id` int(11) NOT NULL,
  `day` varchar(20) NOT NULL,
  `breakfast` varchar(20) NOT NULL,
  `lunch` varchar(20) NOT NULL,
  `dinner` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mess`
--

INSERT INTO `mess` (`id`, `day`, `breakfast`, `lunch`, `dinner`) VALUES
(9, 'Monday', 'Tea and Egg', 'Sabzi', 'Daal'),
(10, 'Tuesday', 'Tea Pratha', 'Nihari', 'Sabzi'),
(11, 'Wednesday', 'Choly Parathy', 'Pulao', 'Kabab'),
(12, 'Thrusday', 'Bread and Egg', 'Daal', 'Biryani'),
(13, 'Friday', 'Bread and Egg', 'Biryani', 'Nihari'),
(14, 'Saturday', 'Tea and Paratha', 'Sabzi', 'Biryani'),
(15, 'Sunday', 'Tea and Egg', 'Pulao', 'Nihari');

-- --------------------------------------------------------

--
-- Table structure for table `pay`
--

CREATE TABLE `pay` (
  `id` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `date` date NOT NULL DEFAULT current_timestamp(),
  `amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pay`
--

INSERT INTO `pay` (`id`, `employeeid`, `date`, `amount`) VALUES
(8, 11, '2022-01-24', 30000),
(9, 12, '2022-01-24', 35000),
(10, 12, '2022-01-24', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `roomNumber` varchar(50) NOT NULL,
  `roomStatus` varchar(50) NOT NULL,
  `maxBeds` int(10) NOT NULL,
  `studentsNumber` int(11) NOT NULL,
  `Fee` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `roomNumber`, `roomStatus`, `maxBeds`, `studentsNumber`, `Fee`) VALUES
(65, 'A150', 'Active', 5, 5, 10000),
(66, 'A160', 'Active', 4, 4, 12000),
(67, 'A161', 'Active', 4, 0, 12000),
(68, 'B101', 'Active', 5, 0, 9000),
(69, 'B102', 'Active', 3, 0, 13000),
(70, 'B103', 'Not Active', 3, 0, 11000),
(71, 'B104', 'Not Active', 3, 0, 11000),
(72, 'B105', 'Active', 3, 0, 10000),
(73, '123', 'Active', 2, 1, 7000);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(10) NOT NULL,
  `cnic` varchar(100) NOT NULL,
  `mobileNo` varchar(20) NOT NULL,
  `name` varchar(200) NOT NULL,
  `father` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address` varchar(200) NOT NULL,
  `college` varchar(100) NOT NULL,
  `roomNo` varchar(10) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `cnic`, `mobileNo`, `name`, `father`, `email`, `password`, `address`, `college`, `roomNo`, `status`) VALUES
(47, '78232-3237-2323', '03088779765', 'Ahmad Ali', 'Ali khan', 'ahmedAli@Mail.com', 'iX7GbAemLOOS', 'Rawalpindi Sadar', 'Riphah University', 'A150', 'Living'),
(48, '378323-23232-2323', '03455912902', 'Nasir Khan Jan', 'kHAN', 'nasir@khan.com', 'iMMo8xck9MTK', 'Islamabad', 'Riphah University', 'A160', 'Living'),
(49, '78374-4349-343', '03455676563', 'Muhammad Ali', 'Kashif', 'ali@mail.net', 'NKdcoB1RikQF', 'Lahore', 'Nust', 'A150', 'Living'),
(50, '78434-343-3434-34', '0349976543', 'Kamran', 'Akmal', 'kamram@gmail.con', 'aVuXO6EUlzg8', 'Karachi', 'PGC', 'A150', 'Living'),
(55, '238283-328392-232', '2378237', 'ueiwuei', 'sYED', 'syedmk600@gmail.com', 'aY6EXTT1fF1x', '781221323', '37283', 'A160', 'Living'),
(56, '8989878987687', '0333524255', 'ali', 'khan', 'syfuxuywvxuy', 'CZYQ9eV1qP1a', 'i 14', 'riphah', '123', 'Living');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cnic` (`cnic`);

--
-- Indexes for table `fee`
--
ALTER TABLE `fee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mess`
--
ALTER TABLE `mess`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pay`
--
ALTER TABLE `pay`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roomNumber` (`roomNumber`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cnic` (`cnic`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `fee`
--
ALTER TABLE `fee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `mess`
--
ALTER TABLE `mess`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `pay`
--
ALTER TABLE `pay`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
