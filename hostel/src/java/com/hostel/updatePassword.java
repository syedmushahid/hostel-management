/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.hostel;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author syedm
 */
public class updatePassword extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      PrintWriter out=response.getWriter();
         HttpSession session = request.getSession(false);
     String userCnic=(String)session.getAttribute("userID");
  String newPass= request.getParameter("newPass");
 String confirmPass= request.getParameter("confirmPass");    
  String currentPass= request.getParameter("currentPass");
 

  if(   confirmPass == null ? newPass != null : !confirmPass.equals(newPass))
  {
      out.print(0);
  }
  else{
      
       Connection con=ConnectionProvider.getCon();
           ResultSet rs;
           try{
            PreparedStatement ps=con.prepareStatement("select * from student where password=? and cnic=? ");
            ps.setString(1, currentPass);
             ps.setString(2, userCnic);
             rs=ps.executeQuery(); 
            if(rs.next())
            {
                
            ps=con.prepareStatement("update student set password=? where cnic=?");
            ps.setString(1, newPass);
             ps.setString(2, userCnic);
            ps.executeUpdate();
            out.print(1);
                          
ps.close();   
    
            }
            else{
                 out.print(2);
            }
           }
           catch(Exception e)
           {
               out.print(e);
           }
              
     
    
    }
    
   
  }
     
}