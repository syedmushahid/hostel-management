/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.hostel;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author syedm
 */
@WebServlet(name = "PayFee", urlPatterns = {"/PayFee"})
public class PayFee extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      PrintWriter out=response.getWriter();
      
      String _feeId= request.getParameter("feeid");

      try{
          
           Connection con=ConnectionProvider.getCon();
           ResultSet rs;
            PreparedStatement ps=con.prepareStatement("update fee set status=? where id=?");
                ps.setString(1, "Paid");
                ps.setString(2, _feeId);
                ps.executeUpdate(); 
          
                
                 out.print(1);
                          
ps.close();   
    
        }
        catch(Exception e){
         out.print(e);
        }
    }
    
}
