/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.hostel;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author syedm
 */
@WebServlet(name = "LoginValodation", urlPatterns = {"/LoginValodation"})
public class LoginValidation extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
     @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      PrintWriter out=response.getWriter();
      
     String email= request.getParameter("email");
      String password= request.getParameter("password");
     
      try{
          
           Connection con=ConnectionProvider.getCon();
           ResultSet rs;
            PreparedStatement ps=con.prepareStatement("select * from student where email=? AND password=?");
                ps.setString(1, email);
                ps.setString(2, password);
                rs = ps.executeQuery(); 
             if (rs.next()) { 
                 HttpSession session = request.getSession(true);
                 session.setAttribute("userEmail",rs.getString("email") );
                 session.setAttribute("userName",rs.getString("name") );
                 session.setAttribute("userID",rs.getString("id"));
                 session.setAttribute("userNumber",rs.getString("mobileNo"));
                 session.setAttribute("address",rs.getString("address"));
                 session.setAttribute("roomNumber",rs.getString("roomNo"));
                 out.print(1);
     
}
             
             else{
                 out.print(2);
             }
rs.close();                                    
ps.close();   
             
                 
            
        }
        catch(Exception e){
         out.print(e);
        }
    }
    
    
//
//    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        
//    }
//
//    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
//    /**
//     * Handles the HTTP <code>GET</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doGet(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//     
//    }
////
//    /**
//     * Handles the HTTP <code>POST</code> method.
//     *
//     * @param request servlet request
//     * @param response servlet response
//     * @throws ServletException if a servlet-specific error occurs
//     * @throws IOException if an I/O error occurs
//     */
//    @Override
//    protected void doPost(HttpServletRequest request, HttpServletResponse response)
//            throws ServletException, IOException {
//        processRequest(request, response);
//    }
//
//    /**
//     * Returns a short description of the servlet.
//     *
//     * @return a String containing servlet description
//     */
//    @Override
//    public String getServletInfo() {
//        return "Short description";
//    }// </editor-fold>

}
