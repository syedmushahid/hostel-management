/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package com.hostel;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author syedm
 */
public class updateInfo extends HttpServlet {
  @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
      PrintWriter out=response.getWriter();
      
      String email= request.getParameter("email");
      String mobile= request.getParameter("mobile");
      String name= request.getParameter("name");
      String address= request.getParameter("address");
       HttpSession session = request.getSession(false);
     String userCnic=(String)session.getAttribute("userID");
      try{
          
           Connection con=ConnectionProvider.getCon();
           ResultSet rs;
            PreparedStatement ps=con.prepareStatement("update student set email=?, name=?,address=?,mobileNo=? where cnic=?");
                ps.setString(1, email);
                ps.setString(2, name);
                ps.setString(3, address);
                ps.setString(4, mobile);
                ps.setString(5,userCnic);
                ps.executeUpdate(); 
          
                
                 session.setAttribute("userEmail",email);
                 session.setAttribute("userName",name );
                 session.setAttribute("userNumber",mobile);
                 session.setAttribute("address",address);
                
                 out.print(1);
                          
ps.close();   
    
        }
        catch(Exception e){
         out.print(e);
        }
    }
    
   
}