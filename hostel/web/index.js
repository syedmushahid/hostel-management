/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

$(document ).ready(function() {
   

$("#loginBtn").on("click",function(){
    event.preventDefault();
  
let email=$("#email").val();
let password=$("#password").val();

if((email!="")&&(password!="")){
    
  
   $.ajax({
  url: "loginvalidation",
  type: "POST",
   data:{
         email:email,
         password:password,
     },
  success: function (data) {
        
       if(data=="1"){
         
           window.location.href = "profile.jsp";
         
       }
       if(data=="2"){
           $("#loginError").text("Wrong Email or Password");
       }
  },
  error: function () {
    console.log("page not found");
  },
});
}
});
  
  
$("#updateInfo").on("click",function(){
    event.preventDefault();
  
let email=$("#updateEmail").val();
let name=$("#updateName").val();
let address=$("#updateAddress").val();
let mobile=$("#updateMobile").val();

if(email=="")
{
    $("#updateEmail").css("border","1px solid red");
}
else{
    $("#updateEmail").css("border","1px solid #ced4da");
}
if(name=="")
{
    $("#updateName").css("border","1px solid red");
}
else{
    $("#updateName").css("border","1px solid #ced4da");
}
if(address=="")
{
    $("#updateAddress").css("border","1px solid red");
}
else{
   $("#updateAddress").css("border","1px solid #ced4da");
}
if(mobile=="")
{
    $("#updateMobile").css("border","1px solid red");
}
else{
  $("#updateMobile").css("border","1px solid #ced4da");
}
if((email!="")&&(name!="")&&(address!="")&&(mobile!="")){
    
  
   $.ajax({
  url: "updateInfo",
  type: "POST",
   data:{
         email:email,
         name:name,
         address:address,
         mobile:mobile,
     },
  success: function (data) {
      
       if(data=="1"){
         
           window.location.href = "profile.jsp";
         
       }
       else{
         alert(data);
       }
  },
  error: function () {
    console.log("page not found");
  },
});
  
  
  
}
    
    else{
        $("#requiredError").text("Required Fields Can't Be Empty")
    }
});


$("#updatePassword").on("click",function(){
    event.preventDefault();
    console.log("Im In function");
let currentPass=$("#currentPass").val();
let newPass=$("#newPass").val();
let confirmPass=$("#confirmPass").val();

if(currentPass=="")
{
   $("#currentPass").css("border","1px solid red");
}
else{
     $("#currentPass").css("border","1px solid #ced4da");
}
if(newPass=="")
{
    $("#newPass").css("border","1px solid red");
}
else{
     $("#newPass").css("border","1px solid #ced4da");
}
if(confirmPass=="")
{
   $("#confirmPass").css("border","1px solid red");
}
else{
     $("#confirmPass").css("border","1px solid #ced4da");
}
if((currentPass!="")&&(newPass!="")&&(confirmPass!="")){
   $.ajax({
  url: "updatePassword",
  type: "POST",
   data:{
         currentPass:currentPass,
         newPass:newPass,
         confirmPass:confirmPass,
     },
  success: function (data) {
      
       if(data=="1"){
         
           window.location.href = "profile.jsp";
         
       }
      else{
         alert(data);
       }
  },
  error: function () {
    console.log("page not found");
  },
});
  
  
  
}
    
});




$(document).on("click","#payFee",function(){
let feeid=$(this).attr("data-feeId");

   $.ajax({
  url: "PayFee",
  type: "POST",
   data:{
         feeid:feeid,
         
     },
  success: function (data) {
        
       if(data=="1"){
         
           window.location.href = "fee.jsp";
         
       }
       else{
           alert(data);
       }
  },
  error: function () {
    console.log("page not found");
  },
});

});
  

});