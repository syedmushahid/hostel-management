<%-- 
    Document   : fee
    Created on : Jan 20, 2022, 12:52:39 AM
    Author     : syedm
--%>

<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.hostel.ConnectionProvider"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 
     
      
     if((String)session.getAttribute("userID")==null){
       response.sendRedirect("/hostel");
     }
   
    %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Fee Challan</title>
    </head>
     <jsp:include page="header.jsp" />
    <body>
      
        <div class="container">
        <% 
      String _userid;
Connection con=ConnectionProvider.getCon();
          _userid=(String)session.getAttribute("userID");
        try{
        ResultSet rs;
            PreparedStatement ps=con.prepareStatement("SELECT student.name, fee.id, student.father,student.cnic,student.roomNo,student.mobileNo,student.address, room.Fee, fee.status,fee.extraCharges,fee.month, fee.expDate from student INNER JOIN room on room.roomNumber=student.roomNo INNER JOIN fee on student.id=fee.studentId where student.id=? order by fee.id desc");
            ps.setString(1,_userid); 
            rs=ps.executeQuery(); 
                
if(rs.next())
{
        while(rs.next()){
        
        %>
       
        <div class="col-md-6 my-5 mx-auto">
            
            <table class="mb-5">
               
                <% if("Un Paid".equals((String)rs.getString("fee.status"))){%>
                <tr>
                <button id="payFee" data-feeId="<%out.print(rs.getString("fee.id")); %>" class="btn btn-danger px-2 mb-1">Pay Now</button>
                       </tr>
                <%}
else{

%>
<tr>
                <button class="btn btn-success px-2 mb-1">Paid</button>
                       </tr>
<%}%>
              
                  <tr style="border-bottom:"none">
                <td>Payment Status</td>
                <td><%out.print(rs.getString("fee.status"));  %></td>
                </tr>
                <tr style="border-bottom:"none">
                <td>Student Name</td>
                <td><%out.print(rs.getString("student.name"));  %></td>
                </tr>
                 <tr>
                <td>Father Name</td>
                
                <td><%out.print(rs.getString("student.father"));  %></td>
                </tr>
                 <tr>
                <td>Room Number</td>
                <td><%out.print(rs.getString("student.roomNo"));  %></td>
                </tr>
                  <tr>
                <td>Mobile Number</td>
                <td><%out.print(rs.getString("student.mobileNo"));  %></td>
                </tr>
                  <tr>
                <td>Cnic Number</td>
                <td><%out.print(rs.getString("student.cnic"));  %></td>
                </tr>
                  <tr>
                <td>Month</td>
                <td><%out.print(rs.getString("fee.month"));  %></td>
                </tr>
                 <tr>
                <td>Expire Data</td>
                <td><%out.print(rs.getString("fee.expDate"));  %></td>
                </tr>
                 <tr>
                <td>Amount</td>
                <td><%out.print(rs.getString("room.Fee"));  %></td>
                </tr>
                  <tr>
                <td>Extra Charges</td>
                <td><%out.print(rs.getString("fee.extraCharges"));  %></td>
                </tr>
                  <tr>
                <td>Total</td>
                <td><b><%out.print(Integer.parseInt(rs.getString("fee.extraCharges"))+Integer.parseInt(rs.getString("room.Fee")));  %><b></td>
                </tr>
                
                
            </table>
         
            
        </div>    
        <% }
}
else{
out.print("<h2>No Fee Challan Found</h2>");
}
    }
    
    catch(Exception e)
    {
    out.print(e);
    }
 %>
        </div>
        
        
    </body>
     <jsp:include page="footer.jsp" />
</html>
