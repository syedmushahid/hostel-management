<%-- 
    Document   : mess
    Created on : Jan 20, 2022, 12:01:06 AM
    Author     : syedm
--%>

<%@page import="java.sql.Statement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="com.hostel.ConnectionProvider"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<% 
     if((String)session.getAttribute("userID")==null){
       response.sendRedirect("/hostel");
     }
    
    %>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mess Menu</title>
          <jsp:include page="header.jsp" />
         
    </head>
    <body>
        
        <%
             ResultSet rs=null;
            try{
           Connection con=ConnectionProvider.getCon();
            Statement st=con.createStatement();
       rs=st.executeQuery("select * from mess");
          
          
            }
            catch(Exception e)
            {
            out.print(e);
            }
        
        %>
        <div class="container pt-5">
<table class="mt-5">
  <h2 class="col-12 text-center">Table Title</h2>
  <thead>
    <tr>
      <th scope="col">Day</th>
      <th scope="col">Break Fast</th>
      <th scope="col">Lunch</th>
      <th scope="col">Diner</th>
    </tr>
  </thead>
  <tbody>
      <% while(rs.next()){
      
      %>
       <tr>
           <td data-label="Day"><b><% out.print(rs.getString("day"));%></b></td>
      <td data-label="Break Fast"><% out.print(rs.getString("breakfast"));%></td>
      <td data-label="Lunch"><% out.print(rs.getString("lunch"));%></td>
      <td data-label="Diner"><% out.print(rs.getString("dinner"));%></td>
    </tr>
        <%  }%>
           
      
      
  </tbody>
         <jsp:include page="footer.jsp" />
    </body>
    </div>
</html>
