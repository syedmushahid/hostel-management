<%-- 
    Document   : profile
    Created on : Jan 18, 2022, 4:05:37 PM
    Author     : syedm
--%>
<%  String userName="";
     String userEmail="";
     String userNumber="";
     String roomNumber="";
     String address="";
     String _id="";
     if((String)session.getAttribute("userID")!=null){
        userName = (String)session.getAttribute("userName"); 
        userEmail = (String)session.getAttribute("userEmail"); 
        _id=(String)session.getAttribute("userID");
        userNumber=(String)session.getAttribute("userNumber");
        address=(String)session.getAttribute("address");
        roomNumber=(String)session.getAttribute("roomNumber");
     }
     else{
         response.sendRedirect("/hostel");
     }
    %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
          <jsp:include page="header.jsp" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Profile | <% out.print(userName); %></title>
    </head>
    <body>
        
        <div class="container">
            <div class="row mt-4">
              
                    <div class="col-md-4">
                        
                        <a href="mess.jsp">
                         <div class="col-12 mb-2 border p-2">
                            <i style="font-size: 152px;" class="fas fa-utensils col-12 text-center "></i>
                            <h6 class="col-12 text-center" style="font-size: 20px">Mess</h6>
                        </div>
                            </a>
                         <a href="fee.jsp">
                        <div  class="col-12 border p-2">
                            <i style="font-size: 152px;" class="fas fa-file-invoice-dollar col-12 text-center "></i>
                            <h6 class="col-12 text-center" style="font-size: 20px">Fees</h6>
                        </div>
                         </a>
                    </div>
                      <div class="col-md-8 profileInfo">
              <div class="card mb-3">
                <div class="card-body ">
                        <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Student Id</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        <% out.print((String)session.getAttribute("userID")); %>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Full Name</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                        <% out.print(userName); %>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Email</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                          <% out.print(userEmail); %>
                    </div>
                  </div>
                  <hr>
                    
                    <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Room Number</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                          <% out.print(roomNumber); %>
                    </div>
                  </div>
                  <hr>
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Phone</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                          <% out.print(userNumber); %>
                    </div>
                  </div>
                  <hr>
                  
                  <div class="row">
                    <div class="col-sm-3">
                      <h6 class="mb-0">Address</h6>
                    </div>
                    <div class="col-sm-9 text-secondary">
                          <% out.print(address); %>
                    </div>
                  </div>
                  <hr>
                  
                   
                      <button class="btn btn-primary " data-toggle="modal" data-target="#editProfileModal">Edit Info</button>
                   
                      <button class="btn btn-primary " data-toggle="modal" data-target="#editPasswordModal">Edit Password</button>
                    
                  
                   
                </div>
              </div>

                    </div>
              
            </div>
            
        </div>
   
                    
                    
                    
                    <!----Modal--->
                    
                    
                   <div class="modal fade" id="editProfileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Profile Information</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="col-12">
					<div class="col-12">
						
							<div class="row mb-3">
								<div class="col-sm-3">
                                                                    <h6 class="mb-0">Full Name<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text"  id="updateName" class="form-control" value=<%out.print(userName); %>>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Email<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" id="updateEmail" class="form-control" value=<%out.print(userEmail); %>>
								</div>
							</div>
							
                                                                
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Mobile<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" id="updateMobile" class="form-control" value=<%out.print(userNumber); %>>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Address<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input type="text" id="updateAddress" class="form-control" value=<%out.print(address); %>>
								</div>
							
							
						</div>
                                                                <div style="color:red" id="requiredError">
                                                                    
                                                                </div>
					</div>
          
          
      </div>
      <div class="modal-footer">
        <button type="button" id="updateInfo" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                   </div>      
                    
         
                                                                
                                                                
                                                                                   
                   <div class="modal fade" id="editPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Edit Profile Password</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="col-12">
					<div class="col-12">
						
							
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Current Password<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
                                                                    <input Id="currentPass" type="text" class="form-control"  required >
								</div>
							</div>
							
                                                                
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">New Password<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input Id="newPass"  type="text" class="form-control"  required>
								</div>
							</div>
							<div class="row mb-3">
								<div class="col-sm-3">
									<h6 class="mb-0">Confirm Password<span style="color:red"> *</span></h6>
								</div>
								<div class="col-sm-9 text-secondary">
									<input Id="confirmPass"  type="text" class="form-control"  required>
								</div>
                                                            
							
							
						</div>
					</div>
          
          
      </div>
      <div class="modal-footer">
        <button type="button" id="updatePassword" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
                   </div>     
                    
                    
    
    
                   
                   </body>
             <jsp:include page="footer.jsp" />
</html>
