<%-- 
    Document   : header.jsp
    Created on : Jan 12, 2022, 9:36:40 PM
    Author     : syedm
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%   String userName="";
     if((String)session.getAttribute("userID")!=null){
        userName = (String)session.getAttribute("userName"); 
     }
    %>

    
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

         <link href="index.css" rel="stylesheet" >

   
    <body>
        <nav class="navbar navbar-light bg-light shadow">
  <div class="container">
      <div >
         
    <a class="navbar-brand" href="/hostel">
      My Hostel
    </a>
      </div>
              <%   if((String)session.getAttribute("userID")!=null){
        userName = (String)session.getAttribute("userName"); 
      %>
      <div>
          <div class="btn-group headermenu">
  <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
       <%out.print("Hi! " +userName); %>
  </button>
  
  <div class="dropdown-menu ">
    <a href="profile.jsp" class="dropdown-item" href="#">Profile</a>
    <a class="dropdown-item" href="mess.jsp">Mess</a>
    <a class="dropdown-item" href="fee.jsp">Fee</a>
    <div class="dropdown-divider"></div>
    <a href="logout" class="dropdown-item" href="#">Logout</a>
  </div>
</div>
       
           
 </div>
  
  
         <%} %>  

  </div>
</nav>
         
 