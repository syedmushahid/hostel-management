<%-- 
    Document   : login
    Created on : Jan 13, 2022, 10:30:38 AM
    Author     : syedm
--%>
<%  String userName="";
     if((String)session.getAttribute("userID")!=null){
         response.sendRedirect("profile.jsp");
     }
    
    %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
      <jsp:include page="header.jsp" />
    <body>     
        <div class="container">
            <div style="height: 80vh" class="row col-12 d-flex justify-content-center align-items-center">
            <div class="col-sm-12 col-md-6 col-lg-5">
    <form class=" col-12 g-3 needs-validation " novalidate>
 
  <div class="col-12">
    <label for="email" class="form-label">Username</label>
    <div class="input-group has-validation">
      <span class="input-group-text" id="inputGroupPrepend">@</span>
      <input type="text" class="form-control" name="email" id="email" aria-describedby="inputGroupPrepend" required>
      <div class="invalid-feedback">
        Please choose a username.
      </div>
    </div>
  </div>
  <div class="col-12">
    <label for="password" class="form-label">Password</label>
    <input type="password" class="form-control" name="password" id="password" required>
    <div class="invalid-feedback">
      Please provide a valid Password.
    </div>
  </div>
 
   <div class="col-12 my-1">
       <label id="loginError" style="color:red">
        
      </label>
     
    </div>
     <div class="col-12 mt-3 d-flex justify-content-center">
    <button class="btn btn-primary" id="loginBtn">Login</button>
  </div>
</form> 
             
            </div>
            </div>
            </div>
           <jsp:include page="footer.jsp" />
           
    </body>
</html>
